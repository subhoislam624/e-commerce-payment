import axios from 'axios'

export const uploadImageToCloudinary = async (image) => {
  const formData = new FormData()
  formData.append('file', image)

  // Determine the upload preset based on the Node environment
  // const uploadPreset = process.env.NODE_ENV === "production" ? "ecodevsupload" : "hola_football";

  formData.append('upload_preset', 'egs2haam')
  formData.append('folder', 'be-goal')

  const { data } = await axios.post(`https://api.cloudinary.com/v1_1/${process.env.NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME}/image/upload`, formData)

  return data.secure_url
}
