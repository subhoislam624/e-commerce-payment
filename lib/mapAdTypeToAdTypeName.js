export const mapAdTypeToAdTypeName = async (adType) => {
  switch (adType) {
    case "admob":
    case "1":
      return "google";
    case "3":
      return "facebook";
    case "4":
      return "startapp";
    default:
      return "";
  }
};
