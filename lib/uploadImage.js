import { BeGoalUrlFile } from "@/lib/api/getAxios";

export default async function uploadImage(image) {
  const formData = new FormData();
  formData.append("image", image);

  try {
    const { data } = await BeGoalUrlFile.post("/api/admin/v2/upload/image/image", formData);
    return data?.data?.path;
  } catch (error) {
    console.error(error);
    return null;
  }
}
