export const getAllActiveAllergens = obj => {
    const allergenSet = new Set();

    function extractAllergens(item) {
        if (Array.isArray(item)) {
            item.forEach(subItem => {
                extractAllergens(subItem);
            });
        } else if (typeof item === 'object' && item !== null) {
            for (const key in item) {
                if (item.hasOwnProperty(key)) {
                    if (key === 'allergens' && Array.isArray(item[key])) {
                        item[key].forEach(allergen => {
                            if (allergen.is_active) {
                                allergenSet.add(allergen.name);
                            }
                        });
                    } else {
                        extractAllergens(item[key]);
                    }
                }
            }
        }
    }

    extractAllergens(obj);

    return Array.from(allergenSet);
};
