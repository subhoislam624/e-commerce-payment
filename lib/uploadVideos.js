import toast from "react-hot-toast";
import { BeGoalUrlFile } from "./api/getAxios";

export const uploadVideos = async videoFiles => {
  const uploadedVideoUrls = [];

  for (const file of videoFiles) {
    try {
      const formData = new FormData();
      formData.append("video", file);

      const { data } = await BeGoalUrlFile.post("/api/v1/upload/video", formData);

      const uploadedVideoUrl = `${process.env.NEXT_PUBLIC_BACKEND_URL}/videos/${data?.data?.filename}`;
      uploadedVideoUrls.push(uploadedVideoUrl);
    } catch (error) {
      console.error("Error uploading video:", error);
      toast.error("Error uploading one or more videos. Please try again.");
    }
  }

  return uploadedVideoUrls;
};
