export default function removeObjectKeys(obj, keysToRemove, keyValueToAdd) {
    const result = { ...obj };

    keysToRemove.forEach(key => {
        if (key in result) {
            delete result[key];
        }
    });

    for (const key in keyValueToAdd) {
        result[key] = keyValueToAdd[key];
    }

    return result;
}
