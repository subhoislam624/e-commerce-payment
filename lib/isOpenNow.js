export const isOpenNow = openingHours => {
    const currentTime = new Date();
    const currentDay = currentTime.toLocaleString('en-US', { weekday: 'long' });
    const currentHour = currentTime.getHours();
    const currentMinute = currentTime.getMinutes();
    const currentTimeInMinutes = currentHour * 60 + currentMinute;

    const todaySchedule = openingHours.hours_schedule.find(schedule => schedule.day.toLowerCase() === currentDay.toLowerCase());

    if (!todaySchedule || !todaySchedule.is_active) {
        return 'Closed';
    }

    const { start, end } = todaySchedule.hours[0];
    const [startHour, startMinute] = start.split(':').map(Number);
    const [endHour, endMinute] = end.split(':').map(Number);
    const startTimeInMinutes = startHour * 60 + startMinute;
    const endTimeInMinutes = endHour * 60 + endMinute;

    if (currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes) {
        return 'Open Now';
    }

    return 'Closed';
};
