import { BeGoalUrlFile } from "./api/getAxios";

async function uploadImageAndUpdateField(index, itemIndex, file, values) {
  try {
    const formData = new FormData();
    formData.append("image", file);

    const { data } = await BeGoalUrlFile.post("/api/v1/upload/image", formData);

    const imageUrl = process.env.NEXT_PUBLIC_BACKEND_URL + "/images/" + data?.data?.filename;

    values.qr_type_info.menu[index].item[itemIndex].image = imageUrl;

    return { status: true, message: "Image Uploaded" };
  } catch (error) {
    return { status: false, message: "Image Upload Failed" };
  }
}

export default uploadImageAndUpdateField;
