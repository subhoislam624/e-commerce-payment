import jwt from 'jsonwebtoken';

export default function getTokenVerify(_token) {
    let is_valid = false;

    try {
        jwt.verify(_token, process.env.NEXT_PUBLIC_SECRET_KEY);
        is_valid = true;
        return is_valid;
    } catch (err) {
        is_valid = false;
        return is_valid;
    }
}
