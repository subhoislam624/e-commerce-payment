import axios from 'axios'

const baseURL = process.env.NEXT_PUBLIC_BACKEND_URL || process.env.BACKEND_URL
const apiKey = process.env.NEXT_PUBLIC_API_KEY || process.env.API_KEY

const baseConfig = {
  baseURL: `${baseURL}/api/admin/v1`,
  timeout: 30000,
  headers: {
    'x-api-key': apiKey,
  },
}

const backend = axios.create(baseConfig)

const rapidConfig = {
  baseURL: process.env.NEXT_PUBLIC_BACKEND_URL,
  timeout: 30000,
  headers: {
    Accept: 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    token: process.env.NEXT_PUBLIC_API_KEY,
  },
}

const BeGoalRapidUrl = axios.create({
  ...rapidConfig,
  headers: {
    ...baseConfig.headers,
    'Content-Type': 'application/json', // JSON content type for non-file requests
  },
})

const BeGoalUrlFile = axios.create({
  ...baseConfig,
  headers: {
    ...baseConfig.headers,
    'Content-Type': 'multipart/form-data', // Multipart form data for file uploads
  },
})

export { BeGoalUrlFile, backend }
