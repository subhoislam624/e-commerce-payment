export default function getRandomCode() {
  const timestamp = Date.now().toString(); // Convert current timestamp to base36
  const randomPortionLength = 10; // Adjust the length as needed
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let randomPortion = "";

  for (let i = 0; i < randomPortionLength; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    randomPortion += characters.charAt(randomIndex);
  }

  return timestamp + randomPortion;
}
