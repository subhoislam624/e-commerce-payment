const jwt = require('jsonwebtoken');

export default function getEmailFromToken(token) {
    try {
        // Decode the token
        const decodedToken = jwt.decode(token);

        // Extract and return the email
        if (decodedToken && decodedToken.email) {
            return decodedToken.email;
        } else {
            throw new Error('Token does not contain email');
        }
    } catch (error) {
        console.error('Error decoding token:', error.message);
        return null;
    }
}
