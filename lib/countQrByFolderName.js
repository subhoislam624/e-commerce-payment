export default function countQrByFolderName(data, folderName) {
    // Convert folderName to lowercase
    folderName = folderName.toLowerCase();

    // Initialize a count variable for the specified folder name
    let folderCount = 0;

    // Iterate through the data array and count objects with the specified folder name
    data.forEach(item => {
        // Convert item.folder to lowercase before checking
        if (item.folder.toLowerCase() === folderName) {
            folderCount++;
        }
    });

    return folderCount;
}
