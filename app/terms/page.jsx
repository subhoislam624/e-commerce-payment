import Logo from '../_components/Logo'

export const metadata = {
  title: 'E-commerce | Terms & Conditions',
}

export default async function Page() {
  // const { data } = await backend.get('/administration-settings')

  return (
    <>
      <header className="flex justify-center p-7">
        <Logo />
      </header>

      <div className="min-h-screen bg-white p-2 lg:p-20">
        <h1 className="my-5 text-center text-3xl font-bold">Terms & Condition</h1>
        <div dangerouslySetInnerHTML={{ __html: '' }}></div>
      </div>
    </>
  )
}
