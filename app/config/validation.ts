import * as Yup from 'yup'

export const categoryFormValidation = Yup.object().shape({
  name: Yup.string().required('Name is required!'),
  // image: Yup.string().url('Invalid URL format').required('Image URL is required!'),
  status: Yup.boolean().required('Status is required!'),
  isPopular: Yup.boolean().required('Popularity status is required!'),
})
export const categoryCreateFormValidation = Yup.object().shape({
  name: Yup.string().required('Name is required!'),
  image: Yup.string().url('Invalid URL format').required('Image URL is required!'),
  status: Yup.boolean().required('Status is required!'),
  isPopular: Yup.boolean().required('Popularity status is required!'),
})

export const productFormValidation = Yup.object().shape({
  name: Yup.string().required('Name is required!'),
  image: Yup.string().required('Image URL is required!').trim(),
  status: Yup.boolean().default(true),
  isPopular: Yup.boolean().default(false),
  price: Yup.number().required('Price is required!').positive('Price must be a positive number!'),
  category: Yup.string().required('Category is required!'),
  quantity: Yup.number().default(0).min(0, 'Quantity cannot be negative!'),
  description: Yup.string().trim(),
})
export const productCreateFormValidation = Yup.object().shape({
  name: Yup.string().required('Name is required!'),
  image: Yup.string().required('Image URL is required!').trim(),
  status: Yup.boolean().default(true),
  isPopular: Yup.boolean().default(false),
  price: Yup.number().required('Price is required!').positive('Price must be a positive number!'),
  category: Yup.string().required('Category is required!'),
  quantity: Yup.number().default(0).min(0, 'Quantity cannot be negative!'),
  description: Yup.string().trim(),
})
