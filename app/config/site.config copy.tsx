import logo from "@public/app_icon.png";
import { Metadata } from "next";

export const siteConfig = {
  title: "Bible Admin",
  description: `Bible Admin panel to manage the app datas`,
  logo: logo,
  icon: logo,
  favicon: logo,
  mode: "light"
  // TODO: favicon
};

export const metaObject = (title?: string, description: string = siteConfig.description): Metadata => {
  return {
    title: title ? `${title} - Bible` : siteConfig.title,
    description
  };
};
