// import logo from "@public/app_icon.png";
import { Metadata } from 'next'
const logo = 'logo'

export const siteConfig = {
  title: 'Jol Tori Admin',
  description: `Jol Tori Admin panel to manage the app datas`,
  logo: logo,
  icon: logo,
  favicon: logo,
  mode: 'light',
  // TODO: favicon
}

export const metaObject = (title?: string, description: string = siteConfig.description): Metadata => {
  return {
    title: title ? `${title} - Jol Tori` : siteConfig.title,
    description,
  }
}
