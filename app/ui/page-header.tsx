import { Title } from 'rizzui'
import Breadcrumb from './breadcrumb'
// import Breadcrumb from "@/components/ui/breadcrumb";

export type PageHeaderTypes = {
  title: string
  breadcrumb: { name: string; href?: string }[]
  className?: string
}

export default function PageHeader({ title, breadcrumb, children, className }: React.PropsWithChildren<PageHeaderTypes>) {
  return (
    <header className={`@container xs:-mt-2 mb-3 lg:mb-4 ${className}`}>
      <div className="@lg:flex-row @lg:items-center @lg:justify-between flex flex-col">
        <div>
          <Title as="h2" className="4xl:text-[26px] mb-2 text-[22px] lg:text-2xl">
            {title}
          </Title>

          <Breadcrumb separator="" separatorVariant="circle" className="flex-wrap">
            {breadcrumb.map((item) => (
              <Breadcrumb.Item key={item.name} {...(item?.href && { href: item?.href })}>
                {item.name}
              </Breadcrumb.Item>
            ))}
          </Breadcrumb>
        </div>
        {children}
      </div>
    </header>
  )
}
