// import CheckoutPage from './../component/Checkout';
import jwt from 'jsonwebtoken'
import { cookies } from 'next/headers'
import CheckoutPage from '../component/Checkout'
import CheckoutOurProduct from '../component/CheckoutOurProduct'

export default function page({ searchParams }) {
  const cookieStore = cookies()

  const userTokenObj = cookieStore.getAll().find((item) => item.name === 'userToken')
  const planTokenObj = cookieStore.getAll().find((item) => item.name === 'planToken')
  if (userTokenObj && planTokenObj) {
    var decodedUser = jwt.verify(userTokenObj.value, process.env.NEXT_PUBLIC_ENCRYPTION_KEY)
    var decodedPlan = jwt.verify(planTokenObj.value, process.env.NEXT_PUBLIC_ENCRYPTION_KEY)
    console.log(decodedUser, 'decodedUser')
    console.log(decodedPlan, 'decodedPlan')
  }

  return <>{decodedPlan?.id ? <CheckoutPage /> : <CheckoutOurProduct />}</>
}
