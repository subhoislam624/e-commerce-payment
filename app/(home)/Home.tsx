'use client'
import Link from 'next/link'
import { FaAmericanSignLanguageInterpreting } from 'react-icons/fa'

import { useGetCategoryQuery } from '@/features/web/category/userApi'
import { useGetProductQuery } from '@/features/web/product/userApi'
import CategoryCard from './component/CategoryCard'
import Navbar from './component/Navbar'
import ProductCard from './component/ProductCard'
import ResonCard from './component/ResonCard'
import Search from './component/Search'
export default function Home() {
  const { data: categoryData, isLoading: categoryLoading, isError: categoryError } = useGetCategoryQuery({})
  const { data: productData, isLoading: productLoading, isError: productError } = useGetProductQuery({})

  const reasons = [
    {
      id: 1,
      icon: <FaAmericanSignLanguageInterpreting className="rounded-full border-2 p-3 text-6xl text-[#FB711D] hover:bg-[#FB711D] hover:text-[#fff]" />,
      title: 'Easy and Fast',
      description: 'It only takes a few seconds to complete a purchase on Jubaly.',
    },
    {
      id: 2,
      icon: <FaAmericanSignLanguageInterpreting className="rounded-full border-2 p-3 text-6xl text-[#FB711D] hover:bg-[#FB711D] hover:text-[#fff]" />,
      title: 'Instant Delivery',
      description:
        'When you purchase on Jubaly, your purchase is delivered directly to your email or game account as soon as your payment is complete.',
    },
    {
      id: 3,
      icon: <FaAmericanSignLanguageInterpreting className="rounded-full border-2 p-3 text-6xl text-[#FB711D] hover:bg-[#FB711D] hover:text-[#fff]" />,
      title: 'Convenient Payment Methods',
      description: 'To ensure your convenience, we have partnered with the most popular providers (sManager) in Bangladesh.',
    },
    {
      id: 3,
      icon: <FaAmericanSignLanguageInterpreting className="rounded-full border-2 p-3 text-6xl text-[#FB711D] hover:bg-[#FB711D] hover:text-[#fff]" />,
      title: 'Customer Support',
      description: 'Our support team is available 24/7 . Message us for support.',
    },
  ]

  const categoryLoadingElements = Array.from({ length: 12 }).map((_, index) => (
    <div key={index} role="status" className="animate-pulse overflow-hidden border-solid">
      <div className="mb-4 h-60 w-48 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      <span className="sr-only">Loading...</span>
    </div>
  ))
  const productLoadingElements = Array.from({ length: 10 }).map((_, index) => (
    <div key={index} role="status" className="animate-pulse overflow-hidden border-solid">
      <div className="mb-4 h-32 w-48 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      <div className="mb-4 h-3 w-48 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      <div className="mb-4 h-3 w-48 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      <div className="mb-4 h-3 w-48 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      <span className="sr-only">Loading...</span>
    </div>
  ))

  return (
    <div className="w-full">
      <div className="mx-auto h-full max-w-[1140px] p-2 ">
        <div>
          <Navbar />
        </div>
        <div className="pt-3 font-semibold leading-5 tracking-wide">
          {/* <marquee behavior="scroll" direction="left" scrollamount="4">
            Our service is open 24 hours, but from 5 am to 6 am Ordering may take some time to receive delivery. Order now, Thank You 😊{' '}
          </marquee> */}
        </div>

        <div className=" mx-auto mb-2 mt-5 w-full max-w-2xl">
          <h1 className="mb-3 text-base font-semibold opacity-60">What are you looking for?</h1>
          <Search />
        </div>

        <div className="mb-10 w-full">
          <h1 className="mb-5 pt-4 text-xl font-semibold">Popular Category</h1>
          {categoryLoading ? (
            <div className="grid grid-cols-2 gap-4 pb-8 md:grid-cols-4 lg:grid-cols-6">{categoryLoadingElements}</div>
          ) : (
            <div className="grid w-full grid-cols-2 gap-4 pb-8 md:grid-cols-4 lg:grid-cols-6">
              {categoryData?.data?.slice(0, 20).map((category: any) => <CategoryCard key={category.id} category={category} />)}
            </div>
          )}
          <div className="mt-2 w-full text-center">
            <Link
              href="/all-category"
              className="rounded-sm bg-[#FB711D] px-4 py-3 text-xs font-semibold uppercase text-[#fff] transition-all duration-200 ease-out hover:border-2 hover:border-[#FB711D] hover:bg-[#fff] hover:text-[#FB711D]"
            >
              view all category
            </Link>
          </div>
        </div>
        <div className="mb-10 w-full">
          <h1 className="pb-5 pt-2 text-xl font-semibold">Popular Products</h1>
          {productLoading ? (
            <div className=" grid grid-cols-2 gap-4 pb-8 md:grid-cols-3 lg:grid-cols-5">{productLoadingElements}</div>
          ) : (
            <div className="grid w-full grid-cols-2 gap-4 pb-8 md:grid-cols-3 lg:grid-cols-5">
              {productData?.data?.slice(0, 20).map((product: any) => <ProductCard key={product.id} product={product} />)}
            </div>
          )}

          <div className="mt-2 w-full text-center">
            <Link
              href="/all-product"
              className="rounded-sm bg-[#FB711D] px-4 py-3 text-xs font-semibold uppercase text-[#fff] transition-all duration-200 ease-out hover:border-2 hover:border-[#FB711D] hover:bg-[#fff] hover:text-[#FB711D]"
            >
              view all product
            </Link>
          </div>
        </div>

        <div className="mb-20 mt-20 grid grid-flow-row gap-4 md:grid-cols-2">
          <div className="flex h-full w-full flex-col items-center justify-start">
            <h2 className="w-full py-4 text-2xl font-semibold">About 24GIFT</h2>
            <p className="w-full text-base font-normal">
              Trusted worldwide, we bring digital prepaid cards & gift cards directly to you. In United States or wherever you are, whenever you need
              them. Because prepaid is the smartest and safest way to pay online. Looking for subscriptions, games, shopping or phone credit? Choose
              from a wide variety of countries and currencies and buy your eGift cards online with fast email delivery!
            </p>
          </div>
          {/* <div className="w-full">
            <Image src="./about-us.jpg" width={100} height={100} className="w-full rounded-md" alt="Picture of the author" />
          </div> */}
        </div>

        <div className="w-full ">
          <div className="pt-10 text-center">
            <h2 className="w-full py-5 text-2xl font-semibold">Why Top Up Games On JUBALY?</h2>
            <p className="px-2 text-sm font-normal md:px-40">
              Millions of gamers or people count on Jubaly every month for a seamless purchase experience when buying game credits or vouchers and
              purchases are delivered to your email or game account instantly. Purchase card now!
            </p>
          </div>
          <div className="my-10 grid grid-cols-1 gap-4 md:grid-cols-2">
            {reasons.map((reason) => (
              <ResonCard key={reason.id} reason={reason} />
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
