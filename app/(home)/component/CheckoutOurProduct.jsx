'use client'
import { loadStripe } from '@stripe/stripe-js'
import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { Loader } from 'rizzui'
import Navbar from './Navbar'
import StripeProductCheckout from './StripeProductCheckout'

export default function CheckoutPage() {
  const stripePromise = loadStripe('pk_test_51NBZG9A3pCcH6cZTVkw41J0EzIljuw7q3gRi4itAHtGugocbciWtat0ZjqjOf2Ot3Pz8DcV9qfVeX0f6hxF6jlno00fbF0IDqC')

  const [isLoading, setIsLoading] = useState(true)

  const cart = useSelector((state) => state.cart)

  const [cartDAta, setCartDAta] = useState([])
  useEffect(() => {
    setCartDAta(cart)
    setIsLoading(false)
  }, [cart])

  // Calculate the total price of all items in the cart
  const totalPrice = cartDAta.reduce((total, item) => {
    return total + item.price * item.quantity
  }, 0)

  // Handle checkout process
  const handleCheckout = async () => {
    setIsLoading(true)

    const stripe = await stripePromise

    // Simulate a call to your backend to create a Checkout session
    const response = await fetch('/api/create-checkout-session', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ items: cartDAta }),
    })

    const session = await response.json()

    // Redirect to Stripe Checkout
    const result = await stripe.redirectToCheckout({
      sessionId: session.id,
    })

    if (result.error) {
      console.error(result.error.message)
    }

    setIsLoading(false)
  }

  return (
    <>
      {isLoading ? (
        <div className="flex h-full min-h-[80vh] items-center justify-center md:min-h-[70vh]">
          <Loader size="xl" />
        </div>
      ) : (
        <>
          {cartDAta.length > 0 ? (
            <StripeProductCheckout cartDAta={cartDAta} />
          ) : (
            <>
              <Navbar />
              <div className="mt-10 flex items-center justify-center ">
                <div className="rounded-md bg-white p-8 text-center shadow-md">
                  <div className="mb-4 text-2xl font-semibold text-gray-800">No Card Data</div>
                  <p className="mb-6 text-gray-600">Your cart is currently empty. Add some items to see them here.</p>
                </div>
              </div>
            </>
          )}
        </>
        // <div className="w-full">
        //   <div className="mx-auto h-full min-h-[70vh] max-w-[1140px] p-2">
        //     <div className="w-full">
        //       <h1 className="px-2 py-4 text-2xl font-semibold">Enter your email for delivery</h1>
        //       <div className="flex w-full items-center gap-2 px-2 font-normal">
        //         Code instantly delivered by email <FcOk />
        //       </div>
        //       <div className="mt-10 flex w-full flex-col gap-5 md:flex-row md:gap-8">
        //         <div className="w-full rounded-lg border border-[#017ACD] bg-[#F3F9FD] p-8">
        //           <div className="flex items-center gap-2 pb-4">
        //             <MdEmail className="text-4xl text-[#FB711D]" /> Send my code(s) by email
        //           </div>
        //           <Input
        //             label="Enter Email"
        //             inputClassName="bg-[#fff]"
        //             labelClassName="text-lg mb-2 font-medium"
        //             placeholder="Enter your email"
        //             size="lg"
        //           />
        //           <p className="py-4 text-base opacity-60">Your order will be delivered to this email address.</p>
        //           <div className="mt-3 flex justify-end">
        //             <button
        //               onClick={handleCheckout}
        //               className="cursor-pointer rounded-sm bg-[#FB711D] px-4 py-2 text-base font-medium text-[#fff] hover:bg-[#fc721dd0]"
        //             >
        //               Checkout
        //             </button>
        //           </div>
        //         </div>
        //         <div className="w-full">
        //           <h4 className="pb-4 text-base font-semibold">Shopping Cart</h4>
        //           <table className="w-full border-[1px]">
        //             <thead></thead>
        //             <tbody>
        //               {cartDAta.map((item) => (
        //                 <tr className="border-[1px] border-[#D7DFE1]" key={item.id}>
        //                   <td className="py-3 pl-4 text-start text-base font-normal opacity-60">{item.name}</td>
        //                   <td className="py-3 pr-4 text-end text-base font-normal opacity-60">{item.price} $</td>
        //                 </tr>
        //               ))}
        //               <tr className="border-[1px] border-[#D7DFE1]">
        //                 <td className="py-3 pl-4 text-start text-lg font-bold">Total</td>
        //                 <td className="py-3 pr-4 text-end text-lg font-bold">{totalPrice.toFixed(2)}$</td>
        //               </tr>
        //             </tbody>
        //           </table>
        //           <br />
        //           <StripeProductCheckout />
        //         </div>
        //       </div>
        //     </div>
        //   </div>
        // </div>
      )}
    </>
  )
}
