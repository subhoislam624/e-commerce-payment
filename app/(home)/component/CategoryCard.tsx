import Link from 'next/link'

export default function CategoryCard({ category }: any) {
  return (
    <Link href={`/all-category/${category?.name.split(' ').join('-')}?id=${category?.id}`}>
      <div className="h-64 w-full overflow-hidden rounded-lg border border-[#fff] bg-white shadow-2xl transition-all duration-200 ease-out hover:shadow-lg">
        <img className="h-full max-h-48 w-full" src={category?.image} alt="" />
        <h2 className="p-3 text-center text-[15px] font-semibold transition-all duration-300 ease-in-out hover:text-[#FB711D]">{category?.name}</h2>
      </div>
    </Link>
  )
}
