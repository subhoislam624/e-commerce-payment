'use client'
import { Elements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import { useEffect, useState } from 'react'
import { Loader } from 'rizzui'
import StripeProductCheckoutForme from './StripeProductCheckoutForme'

const stripePromise = loadStripe('pk_test_51PN5XfI0LFM6TJvPUYcTs8yu8R989SUrmoZ4qL2x3kx8uEqvj6PJfRKZVLRGy8ZtGk524vfEPtKzMWt3Re4u957u00m5DxsPMm')

async function clientSecretGet(data: any, email: any) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': 'f00087d5d0a30ea91816364a4852631e00fc3cd33ddde89dfc5f161365b2933f',
    },
    body: JSON.stringify({
      email: email,
      products: data,
    }),
  }

  const res = await fetch(`${process.env.NEXT_PUBLIC_SERVER_URL}/stripe/payment/intent`, requestOptions)
  if (!res.ok) {
    throw new Error('Failed to fetch client secret')
  }
  return res.json()
}

export default function StripeProductCheckout({ cartDAta }: any) {
  const [options, setOptions] = useState<{ clientSecret: string }>({ clientSecret: '' })
  const [error, setError] = useState('')
  const [emailValidation, setEmailValidation] = useState('')

  function generateRandomEmail() {
    const domains = ['gmail.com', 'yahoo.com', 'hotmail.com']
    const characters = 'abcdefghijklmnopqrstuvwxyz0123456789'

    function getRandomString(length: number) {
      let result = ''
      for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * characters.length))
      }
      return result
    }

    const randomUser = getRandomString(10)
    const randomDomain = domains[Math.floor(Math.random() * domains.length)]

    return `${randomUser}@${randomDomain}`
  }

  // Example usage:
  const userEmail = { email: generateRandomEmail() }
  console.log(userEmail, 'user email')

  const itemDetails = cartDAta.map((item: any) => ({
    id: item.id,
    quantity: item.quantity,
  }))

  // console.log(itemDetails)
  // const planArrayData = [
  //   {
  //     id: '665ebba269bb36daba73490f',
  //     quantity: 3,
  //   },
  // ]
  // const userEmail = { email: 'anonymousUser@gmail.com' }

  useEffect(() => {
    async function fetchData() {
      try {
        const planData: any = await clientSecretGet(itemDetails, userEmail.email)
        console.log(planData, 'planData')

        setOptions({ clientSecret: planData.data.clientSecret })
      } catch (error) {
        console.error('Error fetching client secret:', error)
        setError('Error fetching client secret')
      }
    }

    fetchData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  console.log(options, 'options.clientSecret')

  if (error) {
    return <div>{error}</div>
  }

  if (!options.clientSecret) {
    return (
      <div className="flex h-full min-h-[80vh] items-center justify-center md:min-h-[70vh]">
        <Loader size="xl" />
      </div>
    )
  }

  return (
    <Elements stripe={stripePromise} options={options}>
      {/* <PaymentElement /> */}
      <StripeProductCheckoutForme cartDAta={cartDAta} setEmailValidation={setEmailValidation} emailValidation={emailValidation} />
    </Elements>
  )
}
