import StripePage from './StripePage'

async function fetchPlanData(plan: any) {
  const mainData = {
    headers: {
      'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
      'Content-Type': 'application/json', // Optional, based on your API requirements
    },
  }

  const res = await fetch(`${process.env.NEXT_PUBLIC_CNF_URL}/api/subscriptions/find/${plan}`, {
    credentials: 'include',
    headers: {
      'X-API-KEY': process.env.NEXT_PUBLIC_API_KEY as string,
    },
  })

  if (!res.ok) {
    // Optionally handle errors here
    // For example, you could throw an error or log the issue
    // throw new Error("Failed to fetch plan data");
    console.error('Failed to fetch plan data')
    return null // Return null or handle the error as needed
  }

  return res.json()
}

export default async function CheckPlanId({ planId, decodedUser }: any) {
  let planData = {}
  try {
    planData = await fetchPlanData(planId)
  } catch (error) {
    console.error('Error fetching plan data:', error)
    // return <div>Error fetching plan data</div>;
  }

  return (
    <div>
      <StripePage planInformation={planData} decodedUser={decodedUser} />
    </div>
  )
}
