'use client'

import { CardItem, loadCartState } from '@/features/cart/cart.slice'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import { IoCartOutline } from 'react-icons/io5'
import { useDispatch, useSelector } from 'react-redux'
import SkeletonSpan from './Shemer/SkeletonSpan'
const Navbar = () => {
  const [isLoading, setIsLoading] = useState(true)
  // Selecting cart from global state
  const cart = useSelector((state: any) => state.cart)
  const [cartItem, setCartItem] = useState(null)
  const dispatch = useDispatch()

  // Getting the count of items
  const getItemsCount = () => {
    return cart.reduce((accumulator: any, item: any) => accumulator + item?.quantity, 0)
  }

  useEffect(() => {
    setCartItem(cart.reduce((accumulator: any, item: any) => accumulator + item?.quantity, 0))
    dispatch(CardItem(loadCartState()))
  }, [dispatch, cart])

  useEffect(() => {
    // Simulate a data fetching delay
    const timer = setTimeout(() => {
      setIsLoading(false)
    }, 1500) // Adjust the delay as needed

    return () => clearTimeout(timer)
  }, [])

  return (
    <nav className="w-full">
      {/* <h6>GamesKart</h6>
      <ul>
        <li>
          <Link href='/'>Home</Link>
        </li>
        <li>
          <Link href='/shop'>Shop</Link>
        </li>
        <li>
        <Link href="/cart">
            <p>Cart </p>
          </Link>
        </li>
      </ul> */}

      <div className="h-20 w-full border-b border-[#d7d7d7] bg-[#ffffff]">
        <div className="mx-auto h-full w-full max-w-[1140px] p-2 ">
          <div className="flex h-full w-full items-center justify-between">
            <div className="flex items-center text-3xl font-bold">
              <Link href="/">GamesKart</Link>
            </div>
            <div className="flex items-center gap-x-2">
              <Link href="/cart" className="flex h-full items-center gap-x-1 text-base font-normal opacity-60">
                <span>
                  {/* {isLoading ? <span className=""></span> : `(${cartItem})`} */}
                  {isLoading ? <SkeletonSpan /> : `(${cartItem})`}
                </span>
                <IoCartOutline className="text-2xl" />
              </Link>
              {/* <Link href="#" className="lg:hidden">
                <FaBars className="text-2xl" />
              </Link> */}
            </div>
          </div>
        </div>
      </div>

      <div className="h-12 w-full border-b border-[#d7d7d7] bg-[#F3F6F8] ">
        <div className="mx-auto h-full max-w-[1140px] p-2 ">
          <div className="flex h-full w-full justify-between">
            <ul className="flex items-center gap-x-4">
              <Link href="/" className=" cursor-pointer text-sm font-bold hover:text-[#FB711D]">
                Home
              </Link>
              <Link href="/all-product" className="cursor-pointer text-sm font-bold capitalize hover:text-[#FB711D]">
                All Product
              </Link>
              <Link href="/all-category" className="cursor-pointer text-sm font-bold capitalize hover:text-[#FB711D]">
                All Category
              </Link>
            </ul>
            <div></div>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
