import { PaymentElement, useElements, useStripe } from '@stripe/react-stripe-js'
import { FcOk } from 'react-icons/fc'
import { MdEmail } from 'react-icons/md'
import { Input } from 'rizzui'
import paymentRedirect from './strip/action'

const StriptCheckoutForm = ({ planInformation, decodedUser }: any) => {
  const stripe = useStripe()
  const elements = useElements()

  const handleSubmit = async (event: any) => {
    event.preventDefault()
    if (!stripe || !elements) {
      return
    }

    paymentRedirect(stripe, elements)
    // redirecStripe(stripe,elements)
  }

  const planData = planInformation.data
  let finalPrice

  if (planData && planData.discount) {
    const originalPrice = parseFloat(planData.price)
    const discount = parseFloat(planData.discount)
    const discountAmount: any = ((originalPrice * discount) / 100).toFixed(2)
    finalPrice = (originalPrice - discountAmount).toFixed(2)
  } else {
    finalPrice = '0.00'
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="w-full">
        <div className="mx-auto h-full min-h-[70vh] max-w-[1140px] p-2">
          <div className="w-full">
            <h1 className="px-2 py-4 text-2xl font-semibold">Enter your email for delivery</h1>
            <div className="flex w-full items-center gap-2 px-2 font-normal">
              Code instantly delivered by email <FcOk />
            </div>

            <div className=" mt-10 flex w-full flex-col gap-5 md:flex-row md:gap-8">
              <div className="w-full rounded-lg border border-[#017ACD] bg-[#F3F9FD] p-8">
                <div className="flex items-center gap-2 pb-4">
                  <MdEmail className="text-4xl text-[#FB711D]" /> Send my code(s) by email
                </div>
                <Input
                  label="Enter Email"
                  inputClassName="bg-[#fff]"
                  labelClassName="text-lg mb-2 font-medium"
                  placeholder="Enter your email"
                  size="lg"
                  value={decodedUser.email}
                  required={true}
                />
                <p className="py-4 text-base opacity-60"> You order will be delivered to this email address.</p>
                <div className="mt-3 flex justify-end">
                  <button
                    type="submit"
                    disabled={!stripe}
                    className="cursor-pointer rounded-sm bg-[#FB711D] px-4 py-2 text-base font-medium text-[#fff] hover:bg-[#fc721dd0]"
                  >
                    {' '}
                    Checkout
                  </button>
                </div>
              </div>
              <div className="w-full">
                <h4 className="pb-4 text-base font-semibold">Shopping Cart</h4>
                <table className="w-full border-[1px]">
                  <thead></thead>
                  <tbody>
                    <tr className="border-[1px] border-[#D7DFE1]">
                      <td className="py-3 pl-4 text-start text-base font-normal opacity-60">{planInformation.data.title}</td>
                      <td className="py-3 pr-4 text-end text-base font-normal opacity-60">
                        {planInformation.data.price} {planInformation.data.currency}
                      </td>
                    </tr>
                    <tr className="border-[1px] border-[#D7DFE1]">
                      <td className="py-3 pl-4 text-start text-base font-normal opacity-60">Discount</td>
                      <td className="py-3 pr-4 text-end text-base font-normal opacity-60">
                        {finalPrice} {planInformation.data.currency}
                      </td>
                    </tr>
                    <tr className="border-[1px] border-[#D7DFE1]">
                      <td className="text-bold py-3 pl-4 text-start text-lg">Total</td>
                      <td className="text-bold py-3 pr-4 text-end text-lg">
                        {planInformation.data.discount ? finalPrice : planInformation.data.price} {planInformation.data.currency}
                      </td>
                    </tr>
                  </tbody>
                </table>
                <br />
                <PaymentElement />
                {/* <button disabled={!stripe}>Submit</button> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  )
}

export default StriptCheckoutForm
