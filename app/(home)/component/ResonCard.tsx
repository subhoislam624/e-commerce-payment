export default function ResonCard({ reason }: any) {
  return (
    <div className="flex w-full flex-col items-center justify-center gap-y-2 px-2 text-center ">
      <>{reason?.icon}</>
      <h4 className="text-xl font-semibold md:text-2xl">{reason?.title}</h4>
      <p className="text-sm">{reason?.description}</p>
    </div>
  )
}
