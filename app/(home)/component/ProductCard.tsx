'use client'
import { addToCart } from '@/features/cart/cart.slice'
import Image from 'next/image'
import Link from 'next/link'
import toast from 'react-hot-toast'
import { useDispatch } from 'react-redux'
import { Button } from 'rizzui'
import TostCart from './TostCard'

const ProductCard = ({ product }: any) => {
  const dispatch = useDispatch()
  const handleAddToCart = () => {
    dispatch(addToCart(product))
    // Display a success toast message
    toast.success(<TostCart />)
  }

  return (
    <div className="relative flex min-h-80 w-full flex-col justify-between overflow-hidden rounded-sm border border-[#fff] bg-white shadow-2xl transition-all duration-200 ease-out hover:shadow-lg">
      <Image className="h-40 w-full" src={product.image} alt="" width={500} height={500} />
      <div className="grid w-full grid-cols-1 gap-y-1 px-1 py-3">
        <h2 className="h-full min-h-5 text-[15px] font-medium">{product.name}</h2>
        <Link href={`/(home)/all-category/${product?.name.split(' ').join('-')}?id=${product?.id}`}>
          <h5 className="text-[#FB711D]">{product.name}</h5>
        </Link>
        <p className="font-semibold">$ {product.price}</p>
      </div>
      <Button className="w-full bg-[#1E6DF6]" onClick={() => handleAddToCart()} rounded="none">
        Add to Cart
      </Button>
    </div>
  )
}

export default ProductCard
