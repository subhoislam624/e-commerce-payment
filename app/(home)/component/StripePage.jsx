'use client'
import { Elements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import { useEffect, useState } from 'react'
import { Loader } from 'rizzui'
import StriptCheckoutForm from './StriptCheckoutForm'

const stripePromise = loadStripe('pk_test_51PN5XfI0LFM6TJvPUYcTs8yu8R989SUrmoZ4qL2x3kx8uEqvj6PJfRKZVLRGy8ZtGk524vfEPtKzMWt3Re4u957u00m5DxsPMm')

async function fetchClientSecret(planData) {
  console.log(planData.data._id, '_+_+_+')
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-API-KEY': process.env.NEXT_PUBLIC_API_KEY,
    },
    body: JSON.stringify({
      planId: planData.data._id,
    }),
  }

  const res = await fetch(`${process.env.NEXT_PUBLIC_SERVER_URL}/stripe/intent`, requestOptions)
  if (!res.ok) {
    throw new Error('Failed to fetch client secret')
  }
  return res.json()
}

export default function StripePage({ planInformation, decodedUser }) {
  console.log(planInformation, 'planInformation')
  const [options, setOptions] = useState(null)
  const [error, setError] = useState(null)

  useEffect(() => {
    async function fetchData() {
      try {
        const planData = await fetchClientSecret(planInformation)
        setOptions({
          clientSecret: planData.clientSecret,
        })
      } catch (error) {
        console.error('Error fetching client secret:', error)
        setError('Error fetching client secret')
      }
    }

    fetchData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (error) {
    return <div>{error} stripe page</div>
  }

  if (!options) {
    return (
      <div className="flex h-full min-h-[80vh] items-center justify-center md:min-h-[70vh]">
        <Loader size="xl" />
      </div>
    )
  }

  return (
    <Elements stripe={stripePromise} options={options}>
      <StriptCheckoutForm planInformation={planInformation} decodedUser={decodedUser} />
    </Elements>
  )
}
