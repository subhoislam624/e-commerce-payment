import { PaymentElement, useElements, useStripe } from '@stripe/react-stripe-js'
import { FcOk } from 'react-icons/fc'
import { MdEmail } from 'react-icons/md'
import { Input } from 'rizzui'
import paymentRedirect from './strip/action'

const StripeProductCheckoutForme = ({ cartDAta, emailValidation, setEmailValidation }: any) => {
  const stripe = useStripe()
  const elements = useElements()
  console.log(emailValidation, 'emailValidation')

  const handleSubmit = async (event: any) => {
    event.preventDefault()
    if (!stripe || !elements) {
      return
    }

    if (emailValidation == '') {
      setEmailValidation(true)
      return
    }

    if (emailValidation != '') {
      paymentRedirect(stripe, elements)
    }
  }

  const getTotalPrice = () => {
    return cartDAta.reduce((accumulator: any, item: any) => accumulator + item.quantity * item.price, 0)
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="w-full">
        <div className="mx-auto h-full min-h-[70vh] max-w-[1140px] p-2">
          <div className="w-full">
            <h1 className="px-2 py-4 text-2xl font-semibold">Enter your email for delivery</h1>
            <div className="flex w-full items-center gap-2 px-2 font-normal">
              Code instantly delivered by email <FcOk />
            </div>
            <div className="mt-10 flex w-full flex-col gap-5 md:flex-row md:gap-8">
              <div className="w-full rounded-lg border border-[#017ACD] bg-[#F3F9FD] p-8">
                <div className="flex items-center gap-2 pb-4">
                  <MdEmail className="text-4xl text-[#FB711D]" /> Send my code(s) by email
                </div>
                <Input
                  label="Enter Email"
                  inputClassName={`bg-[#fff] ${emailValidation == true && 'border-red-500 border-[2px]'}`}
                  labelClassName="text-lg mb-2 font-medium"
                  placeholder="Enter your email"
                  size="lg"
                  type="email"
                  required={true}
                  onChange={(e) => setEmailValidation(e.target.value)}
                />

                <p className="py-4 text-base opacity-60">Your order will be delivered to this email address.</p>
                <div className="mt-3 flex justify-end">
                  <button
                    onClick={handleSubmit}
                    type="submit"
                    className="cursor-pointer rounded-sm bg-[#FB711D] px-4 py-2 text-base font-medium text-[#fff] hover:bg-[#fc721dd0]"
                  >
                    Checkout
                  </button>
                </div>
              </div>
              <div className="w-full">
                <h4 className="pb-4 text-base font-semibold">Shopping Cart</h4>
                <table className="w-full border-[1px]">
                  <thead></thead>
                  <tbody>
                    {cartDAta.map((item: any) => (
                      <tr className="border-[1px] border-[#D7DFE1]" key={item.id}>
                        <td className="py-3 pl-4 text-start text-base font-normal opacity-60">{item.name}</td>
                        <td className="py-3 pl-4 text-start text-base font-normal opacity-60">{item.quantity}</td>
                        <td className="py-3 pr-4 text-end text-base font-normal opacity-60">{(item.quantity * item.price).toFixed(2)} $</td>
                      </tr>
                    ))}
                    <tr className="border-[1px] border-[#D7DFE1]">
                      <td className="py-3 pl-4 text-start text-lg font-bold">Total</td>
                      <td className="py-3 pl-4 text-start text-lg font-bold"></td>
                      <td className="py-3 pr-4 text-end text-lg font-bold">{getTotalPrice().toFixed(2)}$</td>
                    </tr>
                  </tbody>
                </table>
                <br />
                <PaymentElement />
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  )
}

export default StripeProductCheckoutForme
