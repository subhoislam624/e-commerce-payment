'use client'

import { decrementQuantity, incrementQuantity, removeFromCart } from '@/features/cart/cart.slice'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Loader } from 'rizzui'
import Navbar from './Navbar'

export default function Cart() {
  const [isLoading, setIsLoading] = useState(true)

  const cart = useSelector((state: any) => state.cart)
  const dispatch = useDispatch()

  const getTotalPrice = () => {
    return cart.reduce((accumulator: any, item: any) => accumulator + item.quantity * item.price, 0)
  }

  const [cartDAta, setCartDAta] = useState([])
  useEffect(() => {
    setCartDAta(cart)
  }, [cart])

  useEffect(() => {
    // Simulating data loading delay
    const timeout = setTimeout(() => {
      setIsLoading(false)
    }, 1500)

    return () => clearTimeout(timeout)
  }, [])

  return (
    <div className="w-full ">
      <div className="mx-auto mb-10 h-full min-h-[80vh] max-w-[1140px] p-2 md:min-h-[70vh] lg:p-0">
        <>
          <div>
            <Navbar />
          </div>
          {isLoading ? (
            // <ShemerTableSkeleton />
            <div className="flex h-full min-h-[80vh] items-center  justify-center md:min-h-[70vh]">
              <Loader size="xl" />
            </div>
          ) : cartDAta.length == 0 ? (
            <div className="flex h-full min-h-[80vh] items-center  justify-center md:min-h-[70vh]">
              {' '}
              <span>Your Cart is Empty!</span>{' '}
            </div>
          ) : (
            <>
              <table className="mt-5 w-full table-fixed">
                <thead className="font-semibold">
                  <tr className="text-left">
                    <th className=" py-2 text-sm font-semibold md:px-4 md:text-base">Image</th>
                    <th className=" py-2 text-sm font-semibold md:px-4 md:text-base">Product</th>
                    <th className=" py-2 text-sm font-semibold md:px-4 md:text-base">Price</th>
                    <th className=" py-2 text-sm font-semibold md:px-4 md:text-base">Qty</th>
                    <th className=" py-2 text-sm font-semibold md:px-4 md:text-base">Actions</th>
                    <th className=" py-2 text-center text-sm font-semibold md:px-4 md:text-base">T.Price</th>
                  </tr>
                </thead>
                <tbody>
                  {cart.map((item: any) => (
                    <tr key={item.id} className="text-center text-xs font-medium md:text-left md:text-sm">
                      <td className="px-1 py-2 md:px-4">
                        {/* Properly render Image component */}
                        <img src={item.image} className="h-12 w-16" alt="" />
                      </td>
                      <td className="px-1 py-2 md:px-4 ">{item.name}</td>
                      <td className="px-1 py-2 md:px-4 ">$ {item.price}</td>
                      <td className="px-1 py-2 md:px-4 ">{item.quantity}</td>
                      <td className="flex flex-wrap gap-x-1 px-1 py-1 md:px-4">
                        <Button onClick={() => dispatch(decrementQuantity(item.id))} size="sm" variant="outline">
                          -
                        </Button>
                        <Button onClick={() => dispatch(incrementQuantity(item.id))} size="sm" variant="outline">
                          +
                        </Button>
                        <Button onClick={() => dispatch(removeFromCart(item.id))} size="sm" variant="outline">
                          x
                        </Button>
                      </td>
                      <td className=" py-2 text-center md:px-4">$ {(item.quantity * item.price).toFixed(2)}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <div className="mt-5 w-full border-t border-[#FB711D]">
                <h2 className="my-5 text-end text-sm font-bold md:pr-12">
                  Grand Total: <span className="text-base">${getTotalPrice().toFixed(2)}</span>
                </h2>
                <div className="mt-2 flex justify-end md:pr-12">
                  <a
                    type="button"
                    href="/checkout"
                    className="cursor-pointer rounded-sm bg-[#FB711D] px-4 py-2 text-base font-medium text-[#fff] hover:bg-[#fc721dd0]"
                  >
                    {' '}
                    Checkout
                  </a>
                </div>
              </div>
            </>
          )}
        </>
      </div>
    </div>
  )
}
