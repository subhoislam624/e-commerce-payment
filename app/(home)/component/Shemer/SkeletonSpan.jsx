import styles from "../Style/SkeletonSpan.module.css";

export default function SkeletonSpan() {
  return (
    <span className={styles.skeleton}></span>
  )
}
