// export default async function paymentRedirect(stripe, elements) {
//   const result = await stripe.confirmPayment({
//     elements,
//     confirmParams: {
//       return_url: 'http://localhost:3000/payments/success',
//     },
//   })
// }

export default async function paymentRedirect(stripe, elements) {
  try {
    const result = await stripe.confirmPayment({
      elements,
      confirmParams: {
        return_url: `${process.env.NEXT_PUBLIC_Domain_URL}/payments/success`,
      },
    })
    if (result && result?.error) {
      window.location.href = '/payments/fail'
    }
  } catch (error) {
    console.error('Payment failed:', error)
  }
}
