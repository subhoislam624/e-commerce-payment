import toast from 'react-hot-toast'
import { useDispatch } from 'react-redux'
import { Button } from 'rizzui'
import TostCart from './TostCard'
import { addToCart } from '@/features/cart/cart.slice'

export default function CategoryProduct({ product }: any) {
  const dispatch = useDispatch()
  const handleAddToCart = () => {
    dispatch(addToCart(product))
    // Display a success toast message
    toast.success(<TostCart />)
  }
  return (
    <div className="flex h-auto w-full flex-col overflow-hidden border-b border-[#D7DFE1] bg-white py-4 hover:bg-[#F3F9FD] md:h-28 md:flex-row md:items-center md:justify-between">
      <div className="flex h-full w-full items-center justify-start gap-x-2">
        <img className="h-[72px] w-28 " src={product?.image} alt="product image" />
        <div className="lg:p-3 ">
          <h2 className=" text-sm font-semibold transition-all duration-300 ease-in-out lg:text-[15px]">{product?.name}</h2>
          <p className="text-base font-semibold text-[#FB711D]">{product?.category.name}</p>
        </div>
      </div>
      <div className="md:pt-auto flex items-center gap-x-1 pt-4 md:gap-x-3">
        <span className="text-nowrap text-base font-bold">$ {product?.price}</span>
        <Button rounded="none" className="bg-blue h-12 w-32  text-sm" onClick={() => handleAddToCart()}>
          Add to Cart
        </Button>
      </div>
    </div>
  )
}
