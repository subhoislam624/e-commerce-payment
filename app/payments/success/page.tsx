import { cookies } from 'next/headers'
import SuccessWrapper from '../_components/SuccessWrapper'

export default function Page() {
  const cookieStore = cookies()
  const planTokenObj = cookieStore.getAll().find((item) => item.name === 'planToken') || {}

  return (
    <section>
      <SuccessWrapper cookie={planTokenObj} />
    </section>
  )
}
