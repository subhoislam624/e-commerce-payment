'use server'
import { cookies } from 'next/headers'

export default async function removeCookies() {
  cookies().delete('userToken')

  cookies().delete('planToken')
}
