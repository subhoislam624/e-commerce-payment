'use client'
import Link from 'next/link'
import { useRouter } from 'next/navigation'
import { useEffect } from 'react'
import removeCookies from './action'

const SuccessWrapper = ({ cookie }: { cookie: any }) => {
  const router = useRouter()

  async function onFly() {
    if (window && typeof window !== 'undefined') {
      const urlStr = window.location.href
      const url = new URL(urlStr)
      const isSucceed = url.searchParams.get('redirect_status')
      if (Object.keys(cookie).length === 0 && !isSucceed) return router.push('/')
      const cartData = JSON.parse(localStorage.getItem('cart') || '{}')
      if (cartData || cookie) {
        if (Object.keys(cartData).length > 0) localStorage.removeItem('cart')
        // fetch(`${process.env.NEXT_PUBLIC_SERVER_URL}/checkout/clear`, {
        //   method: 'POST',
        //   credentials: 'include',
        //   headers: {
        //     'X-API-KEY': process.env.NEXT_PUBLIC_API_KEY as string,
        //   },
        // })
        //   .then((res) => {
        //     console.log(res)
        //   })
        //   .catch((err) => console.log(err))
        removeCookies()
      }
    }
  }

  useEffect(() => {
    onFly()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  return (
    <div className="flex min-h-screen flex-col items-center justify-center bg-gray-100">
      <div className="rounded-md bg-white p-8 text-center shadow-md">
        <h1 className="mb-4 text-3xl font-semibold text-green-600">Payment Successful!</h1>
        <p className="mb-6 text-gray-700">Thank you for your purchase.</p>
        <p className="mb-6 text-gray-700">You will receive a confirmation email shortly.</p>
        <Link href="/" className="inline-block rounded bg-blue-500 px-4 py-2 font-semibold text-white transition duration-300 hover:bg-blue-600">
          Go to Home Page
        </Link>
      </div>
    </div>
  )
}

export default SuccessWrapper
