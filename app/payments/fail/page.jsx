import Link from 'next/link'

const page = () => {
  return (
    <div className="flex min-h-screen flex-col items-center justify-center bg-gray-100">
      <div className="rounded-md bg-white p-8 text-center shadow-md">
        <h1 className="mb-4 text-3xl font-semibold text-red-600">Payment Failed</h1>
        <p className="mb-6 text-gray-700">Your payment was not successful.</p>
        <p className="mb-6 text-gray-700">Please try again or contact support.</p>
        <Link href="/" className="inline-block rounded bg-blue-500 px-4 py-2 font-semibold text-white transition duration-300 hover:bg-blue-600">
          Go to Home Page
        </Link>
      </div>
    </div>
  )
}

export default page
