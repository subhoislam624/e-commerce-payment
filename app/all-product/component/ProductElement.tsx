'use client'
import { useGetProductQuery } from '@/features/web/product/userApi'
import Search from '@/app/(home)/component/Search'
import ProductCard from '@/app/(home)/component/ProductCard'
import Navbar from '@/app/(home)/component/Navbar'
export default function ProductElement() {
  const { data: productData, isLoading: productLoading, isError: productError } = useGetProductQuery({})

  const productLoadingElements = Array.from({ length: 10 }).map((_, index) => (
    <div key={index} role="status" className="animate-pulse overflow-hidden border-solid">
      <div className="mb-4 h-32 w-48 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      <div className="mb-4 h-3 w-48 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      <div className="mb-4 h-3 w-48 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      <div className="mb-4 h-3 w-48 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      <span className="sr-only">Loading...</span>
    </div>
  ))
  return (
    <div className="w-full">
      <div className="mx-auto min-h-[80vh] max-w-[1140px] p-2 lg:p-0">
        <div>
          <Navbar />
        </div>
        <div className=" mx-auto mb-2 mt-5 w-full max-w-2xl">
          <h1 className="mb-3 text-base font-semibold opacity-60">What are you looking for?</h1>
          <Search />
        </div>

        <h1 className="pb-4 pt-4 text-xl font-semibold">All Product</h1>
        {productLoading ? (
          <div className="grid w-full grid-cols-2 gap-4 pb-5 md:grid-cols-3 lg:grid-cols-5">{productLoadingElements}</div>
        ) : (
          <div className="grid w-full grid-cols-2 gap-4 pb-5 md:grid-cols-3 lg:grid-cols-5">
            {productData?.data?.slice(0, 20).map((product: any) => <ProductCard key={product.id} product={product} />)}
          </div>
        )}
      </div>
    </div>
  )
}
