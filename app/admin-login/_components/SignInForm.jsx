'use client'

import { Button } from '@/components/ui/button'
import { CardFooter } from '@/components/ui/card'
import { useAdminLoginMutation } from '@/features/auth/authApi'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import { useRouter } from 'next/navigation'
import { useState } from 'react'
import toast from 'react-hot-toast'
import { BsEye, BsEyeSlash } from 'react-icons/bs'
import { ImSpinner6 } from 'react-icons/im'
import * as Yup from 'yup'

export default function SignInForm() {
  const { push } = useRouter()
  const [showPassword, setShowPassword] = useState(false)
  const [isSubmitting, setIsSubmitting] = useState(false)

  const initialValues = {
    email: '',
    password: '',
  }

  const validationSchema = Yup.object().shape({
    email: Yup.string().email('Invalid Email!').required('Required'),
    password: Yup.string().required('Required'),
  })
  const [adminLogin] = useAdminLoginMutation()
  const onSubmit = async (values) => {
    // setIsSubmitting(true)

    // const auth = await signIn('credentials', {
    //   ...values,
    //   redirect: false,
    // })

    adminLogin(values)
      .then((response) => {
        console.log(response, 'data')
        // Handle successful login
        if (response.error) {
          toast.error(response.error.data?.message || 'Something went wrong')
        } else {
          toast.success('Login successful')
          push('/admin/dashboard')
        }
      })
      .catch((err) => {
        // Handle error
        console.log(err, 'error')
        toast.error(err.message || 'An error occurred')
      })
      .finally(() => {
        // toast.success('Welcome back!')
        // setIsSubmitting(false)
        console.log('Login attempt finished')
      })
    // if (auth?.error) {
    //   toast.error(auth?.error)
    // } else if (auth?.ok) {
    //   toast.success('Welcome back!')
    //   push('/admin/dashboard')
    // }
  }

  return (
    <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
      {() => (
        <Form>
          <div className="my-7 w-full">
            <label className="label">
              <span className="label-text font-bold">
                Email<span className="mx-1 text-red-600">*</span>
                <ErrorMessage name="email" component={({ children }) => <span className="text-sm text-red-600">({children})</span>} />
              </span>
            </label>
            <Field name="email">
              {({ field, meta }) => {
                return (
                  <>
                    <input
                      type="email"
                      className={`${meta.touched && meta.error ? 'input-error' : 'input-neutral'} input input-bordered w-full`}
                      {...field}
                    />
                  </>
                )
              }}
            </Field>
          </div>

          <div className=" my-7 w-full">
            <label className="label">
              <span className="label-text font-bold">
                Password<span className="mx-1 text-red-600">*</span>
                <ErrorMessage name="password" component={({ children }) => <span className="text-sm text-red-600">({children})</span>} />
              </span>
            </label>
            <Field name="password">
              {({ field, meta }) => {
                return (
                  <div className="relative">
                    <input
                      type={showPassword ? 'text' : 'password'}
                      className={`${meta.touched && meta.error ? 'input-error' : 'input-neutral'} input input-bordered w-full`}
                      {...field}
                    />
                    {showPassword ? (
                      <BsEye onClick={() => setShowPassword(false)} className="absolute right-3 top-3 cursor-pointer text-2xl" />
                    ) : (
                      <BsEyeSlash onClick={() => setShowPassword(true)} className="absolute right-3 top-3 cursor-pointer text-2xl" />
                    )}
                  </div>
                )
              }}
            </Field>
          </div>

          <CardFooter className="mt-7 flex justify-between">
            <Button
              type="submit"
              className="btn-primary w-full transition-all duration-300 disabled:bg-violet-800 disabled:text-violet-100"
              disabled={isSubmitting}
            >
              {isSubmitting ? <ImSpinner6 className="animate-spin" /> : <span className="mr-3 text-lg font-semibold">Login</span>}
            </Button>
          </CardFooter>
        </Form>
      )}
    </Formik>
  )
}
