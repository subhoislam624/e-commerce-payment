import { Card, CardContent, CardDescription, CardHeader, CardTitle } from '@/components/ui/card'
import { Flex } from '@radix-ui/themes'
import Logo from '../_components/Logo'
import SignInForm from './_components/SignInForm'

export default async function Page() {
  return (
    <Flex justify="center" align="center" height="100vh">
      <Card className="w-[350px]">
        <CardHeader>
          <CardTitle className="flex justify-center">
            <Logo />
          </CardTitle>
          <CardDescription className="text-center">Admin Login</CardDescription>
        </CardHeader>
        <CardContent>
          <SignInForm />
        </CardContent>
      </Card>
    </Flex>
  )
}
