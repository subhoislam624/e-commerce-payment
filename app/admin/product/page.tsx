import Link from 'next/link'
import UserTable from './_components/ContentTable'
import { HiPlus } from 'react-icons/hi'
import ContentTable from './_components/ContentTable'

export default function Page() {
  return (
    <>
      <ContentTable />
    </>
  )
}
