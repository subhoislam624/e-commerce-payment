'use client'

import CustomModal from '@/app/admin/components/CustomModal'
import InputField from '@/app/admin/components/Formik/InputField'
import StatusBar from '@/app/admin/components/Formik/StatusBar'
import { productCreateFormValidation, productFormValidation } from '@/app/config/validation'
import { useCategoryGetAllApiQuery } from '@/features/admin/category/categoryApi'
import { useCreateApiMutation, useGetByIdApiQuery, useUpdateApiMutation } from '@/features/admin/product/productApi'
import { Form, Formik } from 'formik'
import toast from 'react-hot-toast'
import { Button } from 'rizzui'
import InputTextarea from '../../components/Formik/InputTextArea '
import SelectField from '../../components/Formik/SelectField'

export default function CreateAndUpdateModal({
  edit,
  id,
  setModalState,
  modalState,
}: {
  edit?: boolean
  id?: string
  setModalState: any
  modalState: boolean
}) {
  const [updateApi] = useUpdateApiMutation()
  const [createApi] = useCreateApiMutation()
  const { data } = useGetByIdApiQuery(id, { skip: !edit })

  const handleSubmit = (values: any) => {
    console.log(values, 'submit values')
    if (edit) {
      updateApi({ id, data: values })
        .then((res: any) => {
          if (!res?.error) {
            toast.success(res.data.message || 'created successfully')
            // router.push(routes.admin.user.home)
            setModalState(false)
          } else {
            toast.error(res.error.data.message || 'Something went wrong')
          }
        })
        .catch((err: any) => console.log('user ', err))
    } else {
      createApi(values)
        .then((res: any) => {
          if (!res?.error) {
            toast.success(res.data.message || 'created successfully')
            // router.push(routes.admin.user.home)
            setModalState(false)
          } else {
            toast.error(res.error.data.message || 'Something went wrong')
          }
        })
        .catch((err: any) => console.log(err))
    }
  }

  let initialValues = {
    name: edit ? data?.data?.name ?? '' : '',
    image: edit ? data?.data?.image ?? '' : '',
    status: edit ? data?.data?.status ?? true : true,
    isPopular: edit ? data?.data?.isPopular ?? false : false,
    price: edit ? data?.data?.price ?? 0 : 0,
    category: edit ? data?.data?.category?.id ?? '' : '',
    quantity: edit ? data?.data?.quantity ?? 0 : 0,
    description: edit ? data?.data?.description ?? '' : '',
  }

  const { isLoading, data: categoryData } = useCategoryGetAllApiQuery({})
  console.log(data, '================================')

  // if(isLoading) {
  //  const transformedOptionsData = categoryData?.data?.map((item: any) => ({
  //       label: item.name,
  //       value: item._id,
  //     }));
  // }

  return (
    <>
      <CustomModal modalState={modalState} setModalState={setModalState} title={edit ? 'Update product' : 'Create product'}>
        <div className="mt-8 w-full  rounded-md border px-5 py-8">
          <Formik
            initialValues={initialValues}
            validationSchema={edit ? productFormValidation : productCreateFormValidation}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            {({ values, setFieldValue }) => (
              <Form>
                <InputField name="name" label="Name" placeholder="Name" />
                <InputField name="image" label="Image" placeholder="image" />
                <InputField name="price" label="Price" placeholder="Price" type="number" />
                <InputField name="quantity" label="Quantity" placeholder="Quantity" type="number" />

                <div className="mb-3">
                  <SelectField
                    name="category"
                    label="Category"
                    options={categoryData?.data.map((item: any) => ({
                      label: item.name,
                      value: item.id,
                    }))}
                    value={values.category}
                    onChange={(option: { label: string; value: string }) => setFieldValue('category', option.value)}
                  />
                </div>
                <InputTextarea
                  name="description"
                  label="Description"
                  placeholder="Write you Description..."
                  setFieldValue={setFieldValue}
                  value={values.description}
                />

                <StatusBar setFieldValue={setFieldValue} checkedStatus={values.status} />
                <StatusBar setFieldValue={setFieldValue} checkedStatus={values.isPopular} statusLabel="isPopular" statusName="isPopular" />

                <div className="flex justify-end">
                  <Button type="submit" className="mt-4">
                    Submit
                  </Button>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </CustomModal>
    </>
  )
}

// use
{
  /* <Button onClick={() => modelHandelFunction(false, '')}>Custom Style Modal</Button> */
}

{
  /* <CreateUser setModalState={setModalState} modalState={modalState} edit={modalEdit} id={modalUpdateStateData?.id} /> */
}
