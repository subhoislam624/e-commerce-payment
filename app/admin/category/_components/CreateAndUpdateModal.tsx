'use client'

import CustomModal from '@/app/admin/components/CustomModal'
import InputField from '@/app/admin/components/Formik/InputField'
import StatusBar from '@/app/admin/components/Formik/StatusBar'
import { categoryCreateFormValidation, categoryFormValidation } from '@/app/config/validation'
import { useCategoryCreateApiMutation, useCategoryGetByIdApiQuery, useCategoryUpdateApiMutation } from '@/features/admin/category/categoryApi'
import { Form, Formik } from 'formik'
import toast from 'react-hot-toast'
import { Button } from 'rizzui'

export default function CreateAndUpdateModal({
  edit,
  id,
  setModalState,
  modalState,
}: {
  edit?: boolean
  id?: string
  setModalState: any
  modalState: boolean
}) {
  const [updateApi] = useCategoryUpdateApiMutation()
  const [createApi] = useCategoryCreateApiMutation()
  const { data } = useCategoryGetByIdApiQuery(id, { skip: !edit })

  const handleSubmit = (values: any) => {
    console.log(values, 'submit values')

    if (edit) {
      updateApi({ id, data: values })
        .then((res: any) => {
          if (!res?.error) {
            toast.success(res.data.message || 'created successfully')
            // router.push(routes.admin.user.home)
            setModalState(false)
          } else {
            toast.error(res.error.data.message || 'Something went wrong')
          }
        })
        .catch((err: any) => console.log('user ', err))
    } else {
      createApi(values)
        .then((res: any) => {
          if (!res?.error) {
            toast.success(res.data.message || 'created successfully')
            // router.push(routes.admin.user.home)
            setModalState(false)
          } else {
            toast.error(res.error.data.message || 'Something went wrong')
          }
        })
        .catch((err: any) => console.log(err))
    }
  }

  let initialValues = {
    name: '',
    image: '',
    status: true,
    isPopular: true,
  }

  if (edit) {
    initialValues = {
      name: data?.data?.name,
      image: data?.data?.image,
      status: data?.data?.status,
      isPopular: data?.data?.isPopular,
    }
  }
  return (
    <>
      <CustomModal modalState={modalState} setModalState={setModalState} title={edit ? 'Update category' : 'Create category'}>
        <div className="mt-8 w-full  rounded-md border px-5 py-8">
          <Formik
            initialValues={initialValues}
            validationSchema={edit ? categoryFormValidation : categoryCreateFormValidation}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            {({ values, setFieldValue }) => (
              <Form>
                <InputField name="name" label="Name" placeholder="Name" />
                <InputField name="image" label="Image" placeholder="image" />

                <StatusBar setFieldValue={setFieldValue} checkedStatus={values.status} />
                <StatusBar setFieldValue={setFieldValue} checkedStatus={values.isPopular} statusLabel="isPopular" statusName="isPopular" />

                <div className="flex justify-end">
                  <Button type="submit" className="mt-4">
                    Submit
                  </Button>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </CustomModal>
    </>
  )
}

// use
{
  /* <Button onClick={() => modelHandelFunction(false, '')}>Custom Style Modal</Button> */
}

{
  /* <CreateUser setModalState={setModalState} modalState={modalState} edit={modalEdit} id={modalUpdateStateData?.id} /> */
}
