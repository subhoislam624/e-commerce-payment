'use client'
import CustomTable from '@/app/admin/components/CustomTable'
import PageLoading from '@/app/admin/components/PageLoading'
import { routes } from '@/app/config/routes'
import { useCategoryGetAllApiQuery, useCategoryDeleteApiMutation } from '@/features/admin/category/categoryApi'
import { ICategory } from '@/types'
import { MRT_PaginationState, type MRT_ColumnDef } from 'mantine-react-table'
import Image from 'next/image'
import { useMemo, useState } from 'react'
import { HiPlus } from 'react-icons/hi'
import { Button } from 'rizzui'
import CreateAndUpdateModal from './CreateAndUpdateModal'
import PageHeader from '@/app/ui/page-header'

export default function ContentTable() {
  const [pagination, setPagination] = useState<MRT_PaginationState>({
    pageIndex: 0,
    pageSize: 10,
  })
  const [query, setQuery] = useState({})
  const { isLoading, data: categoryData } = useCategoryGetAllApiQuery({
    page: pagination.pageIndex + 1,
    limit: pagination.pageSize,
    ...(query && { ...query }),
  })

  const [deleteApi] = useCategoryDeleteApiMutation()

  const data: ICategory[] = categoryData?.data?.docs ? categoryData?.data?.docs : categoryData?.data || []

  const columns = useMemo<MRT_ColumnDef<ICategory>[]>(
    () => [
      {
        accessorFn: (originalRow) => (
          <div className="space-2 flex items-center justify-start">
            <Image className="mr-2 h-10 w-10 rounded-full " src={originalRow?.image} width={100} height={100} alt={originalRow?.name} />
            <span>{originalRow?.name}</span>
          </div>
        ),
        id: 'name',
        header: 'Name',
      },
      // {
      //   accessorFn: (originalRow) => originalRow?.name,
      //   id: 'name',
      //   header: 'Name',
      // },

      {
        accessorFn: (originalRow) =>
          originalRow?.status ? (
            <Button className="bg-success text-white">Active</Button>
          ) : (
            <Button className="bg-danger text-black">Inactive</Button>
          ),
        id: 'status',
        header: 'Status',
      },
      {
        accessorFn: (originalRow) =>
          originalRow?.isPopular ? (
            <Button className="bg-success text-white">Active</Button>
          ) : (
            <Button className="bg-danger text-black">Inactive</Button>
          ),
        id: 'isPopular',
        header: 'Popular',
      },
    ],
    [],
  )
  // if (isLoading) return <PageLoading />

  // this code modal handel modal create and update
  const [modalState, setModalState] = useState(false)
  const [modalEdit, setModalEdit] = useState(false)
  const [modalUpdateStateData, setModalUpdateStateData] = useState<any>()
  function modelHandelFunction(edit = false, data?: any) {
    setModalState(true)
    setModalUpdateStateData(data)
    setModalEdit(edit)
  }
  // <Createcategory setModalState={setModalState} modalState={modalState} edit={modalEdit} id={modalUpdateStateData?.id} />
  // <Button onClick={() => modelHandelFunction(false, '')}> <HiPlus className="text-lg" /> Add category</Button>

  // this code modal handel create and update
  const pageHeader = {
    title: 'Category Table',
    breadcrumb: [
      {
        href: routes.admin.dashboard,
        name: 'Dashboard',
      },
      {
        name: 'Product',
      },
    ],
  }
  return (
    <>
      <PageHeader title={pageHeader.title} breadcrumb={pageHeader.breadcrumb}>
        <div className="flex justify-end"></div>
      </PageHeader>
      <div className="card card-body mb-5 w-full p-5 font-bold text-black shadow-lg"></div>
      <div className="mb-7 flex justify-end">
        <Button
          className="hover:bg-primary-dark flex items-center space-x-2 rounded-md bg-primary px-3 py-2 text-white transition-all duration-300"
          onClick={() => modelHandelFunction(false, '')}
        >
          <HiPlus className="text-lg" /> Add Category
        </Button>
      </div>
      <CustomTable
        columns={columns as []}
        isLoading={isLoading}
        data={isLoading ? PageLoading : data}
        pagination={pagination}
        setPagination={setPagination}
        totalDocs={categoryData?.data?.totalDocs}
        deleteRow={deleteApi as any}
        editRow={routes?.admin?.category?.edit}
        modelHandelFunction={modelHandelFunction}
        isEdit={false}
        setQuery={setQuery}
        searchField="email"
      />
      <CreateAndUpdateModal setModalState={setModalState} modalState={modalState} edit={modalEdit} id={modalUpdateStateData?.id} />
    </>
  )
}
