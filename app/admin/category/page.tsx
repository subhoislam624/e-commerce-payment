import ContentTable from './_components/ContentTable'

export default function Page() {
  return (
    <>
      <ContentTable />
    </>
  )
}
