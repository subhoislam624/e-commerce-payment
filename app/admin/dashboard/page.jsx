import BreadCrumb from '@/app/components/Global/BreadCrumb'

export default async function Page() {
  const breadMenu = [
    {
      path: 'Dashboard',
      link: '/admin/dashboard',
    },
  ]

  return (
    <>
      <BreadCrumb breadMenu={breadMenu} />
    </>
  )
}
