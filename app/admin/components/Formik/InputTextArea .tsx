import { Field } from 'formik'
import { useEffect } from 'react'
import { Textarea } from 'rizzui'

export default function InputTextarea({
  name,
  label,
  placeholder = 'Write you message...',
  ref,
  required = true,
  value, // Add value prop
  type = 'text',
  setFieldValue,
}: {
  name: string
  label: string
  placeholder?: string
  ref?: any
  required?: boolean
  value?: any // Modify prop types
  type?: string
  setFieldValue?: any
}) {
  useEffect(() => {
    setFieldValue(name, value)
  }, [setFieldValue, value, name])

  const handleChange = (e: any) => {
    setFieldValue(name, e.target.value)
  }
  return (
    <div className="bg-white">
      <Field name={name}>
        {({ field, meta }: { field: any; meta: any }) => (
          <>
            <Textarea
              name={name}
              label={label}
              placeholder={placeholder}
              onChange={handleChange}
              value={value}
              required={required}
              className={`${meta.touched && meta.error ? '!border-red-500 ring-0' : 'border-gray-200'}`}
            />
            {/* {errors && touched && <div className="mt-1 text-xs text-red-500">{errors}</div>} */}
          </>
        )}
      </Field>
    </div>
  )
}

{
  /*<InputTextarea
                  name="description"
                  label="Description"
                  placeholder="Write you message Description..."
                  setFieldValue={setFieldValue}
                  value={values.description}
                /> */
}
