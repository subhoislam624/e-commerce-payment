// export default StatusBar;
import React, { useEffect, useState } from 'react'
import { Switch } from 'rizzui'

interface StatusBarProps {
  setFieldValue: (field: string, value: any) => void
  checkedStatus?: boolean
  statusName?: string
  statusLabel?: string
}

const StatusBar: React.FC<StatusBarProps> = ({ setFieldValue, checkedStatus = true, statusName = 'status', statusLabel = 'Status' }) => {
  const [status, setStatus] = useState(checkedStatus)

  useEffect(() => {
    setStatus(checkedStatus)
    setFieldValue(statusName, checkedStatus)
  }, [checkedStatus, setFieldValue, statusName])

  const handleToggle = () => {
    const newStatus = !status
    setStatus(newStatus)
    setFieldValue(statusName, newStatus)
  }

  return (
    <div className="status-bar">
      {/* <Switch label="Status" variant="outline" labelPlacement="right" checked={status} onChange={handleToggle} /> */}
      <Switch
        label={statusLabel}
        switchKnobClassName="bg-[#3872FA]"
        variant="outline"
        labelPlacement="right"
        checked={status}
        onChange={handleToggle}
      />
    </div>
  )
}

export default StatusBar

{
  /* <StatusBar setFieldValue={setFieldValue} /> */
}

// v2 ========== active inactive =================

// import React, { useEffect, useState } from 'react'
// import { Switch } from 'rizzui'

// interface StatusBarProps {
//   setFieldValue: (field: string, value: any) => void
//   checkedStatus?: string // Assuming checkedStatus is a string 'active' or 'inactive'
//   statusName?: string

// }

// const StatusBar: React.FC<StatusBarProps> = ({ setFieldValue, checkedStatus = 'active' ,statusName = 'status' }: any) => {
//   const [status, setStatus] = useState(checkedStatus)

//   useEffect(() => {
//     setStatus(checkedStatus)
//     setFieldValue('status', checkedStatus)
//   }, [checkedStatus, setFieldValue])

//   const handleToggle = () => {
//     const newStatus = status === 'active' ? 'inactive' : 'active'
//     setStatus(newStatus)
//     setFieldValue(statusName, newStatus)
//   }

//   return (
//     <div className="status-bar">
//       <Switch
//         label="Status"
//         switchKnobClassName="bg-[#3872FA]"
//         variant="outline"
//         labelPlacement="right"
//         checked={status === 'active'}
//         onChange={handleToggle}
//       />
//     </div>
//   )
// }

// export default StatusBar

// {
//   /* <StatusBar setFieldValue={setFieldValue} /> */
// }
