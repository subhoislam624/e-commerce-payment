import { Field } from 'formik'
import { Input } from 'rizzui'

export default function InputField({
  name,
  label,
  placeholder,
  required = true,
  type = 'text',
  className = 'w-full mb-3',
}: {
  name: string
  label: string
  placeholder?: string
  required?: boolean
  type?: string
  className?: string
}) {
  return (
    <div className={className}>
      <Field name={name}>
        {({ field, meta }: { field: any; meta: any }) => (
          <Input
            {...field}
            label={
              <span>
                {label} {required && <span className="mb-2 text-red-600">*</span>}
              </span>
            }
            type={type}
            placeholder={placeholder}
            autoComplete="off"
            // error={meta.touched && meta.error && <div className="mt-1 text-xs text-red-500">{meta.error}</div>}
            inputClassName={`${meta.touched && meta.error ? '!border-red-500 ring-0' : 'border-gray-200'}`}
          />
        )}
      </Field>
    </div>
  )
}

// this is the use input field component
{
  /* <InputField name="name" label="Name" placeholder="ex. Ring Road Branch" /> */
}
