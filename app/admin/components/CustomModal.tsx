'use client'
import { ImCross } from 'react-icons/im'
import { ActionIcon, Modal, Text } from 'rizzui'

export default function CustomModal({
  modalState,
  setModalState,
  title = 'Welcome',
  children,
}: {
  modalState: boolean
  setModalState: any
  title?: string
  children: React.ReactNode
}) {
  return (
    <>
      <Modal isOpen={modalState} onClose={() => setModalState(false)} overlayClassName="backdrop-blur" containerClassName="!max-w-4xl !shadow-2xl">
        <div className="m-auto rounded-lg bg-white px-7 pb-8 pt-6">
          <div className="mb-7 flex items-center justify-between">
            <Text className="font-bold">{title}</Text>
            <ActionIcon size="sm" variant="text" onClick={() => setModalState(false)}>
              <ImCross />
            </ActionIcon>
          </div>
          <div className="px-2 py-2 shadow-lg">{children}</div>
        </div>
      </Modal>
    </>
  )
}
