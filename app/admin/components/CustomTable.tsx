import { debounce } from '@/app/utils/debounce'
import { ActionIcon, Box } from '@mantine/core'
import { MRT_PaginationState, MantineReactTable, useMantineReactTable } from 'mantine-react-table'
import Link from 'next/link'
import { BiEdit } from 'react-icons/bi'
import { Button } from 'rizzui'
import DeleteConfirmation from './deleteModal/DeleteConfirmation'

//@ts-ignore
interface Column<T> {
  accessorFn: (originalRow: Record<string, any>) => any
  id: string
  header: string
}

interface IPagination {
  pageIndex: number
  pageSize: number
}

interface CustomTableProps<T> {
  columns: Column<T>[]
  data: any
  pagination: IPagination
  totalDocs?: number
  setPagination: (pagination: MRT_PaginationState | ((oldState: MRT_PaginationState) => MRT_PaginationState)) => void
  deleteRow?: (id: string) => void
  editRow?: (id: number | string) => string
  setQuery: Function
  isLoading: boolean
  searchField?: string
  rowActions?: any
  isEdit?: boolean
  editIcon?: React.ReactNode
  renderAction?: boolean
  disableSearch?: boolean
  modelHandelFunction?: any
}

export default function CustomTable<T>({
  columns,
  data,
  pagination,
  setPagination,
  totalDocs,
  deleteRow,
  editRow,
  isLoading,
  setQuery,
  searchField = 'title',
  rowActions,
  renderAction = true,
  isEdit = true,
  disableSearch = true,
  modelHandelFunction,
  editIcon = <BiEdit size={20} />,
}: CustomTableProps<T>) {
  const handleFilter = (keyword: string) => {
    // @ts-ignore
    if (setQuery) {
      if (keyword && keyword?.trim().length > 3) {
        let obj: any = {}
        obj[searchField] = keyword
        setQuery(obj)
      }
    }
  }

  // modelHandelFunction

  // modelHandelFunction
  const debounceFilter = debounce(handleFilter, 500)

  const table = useMantineReactTable({
    columns,
    data,
    rowCount: totalDocs,
    enableRowSelection: false,
    enableRowActions: renderAction,
    positionActionsColumn: 'last',
    enableColumnOrdering: true,
    manualPagination: true,
    enableFilterMatchHighlighting: false,
    onPaginationChange: setPagination,
    state: {
      isLoading: isLoading,
      showProgressBars: isLoading,
      pagination,
    },
    initialState: {
      showGlobalFilter: disableSearch,
    },
    enableToolbarInternalActions: false,
    enableColumnFilters: false,
    enableColumnActions: false,
    enableColumnDragging: false,
    enableSorting: false,
    mantinePaginationProps: {
      rowsPerPageOptions: ['5', '10', '25', '40'],
      withEdges: false,
    },
    mantineSearchTextInputProps: {
      placeholder: 'Please type atleast 3 words',
    },
    onGlobalFilterChange: debounceFilter,
    renderRowActions: rowActions
      ? rowActions
      : ({ row }) => (
          <Box sx={{ display: 'flex', flexWrap: 'nowrap', gap: '8px' }}>
            {isEdit && (
              <ActionIcon color="orange">
                <Link href={editRow ? editRow(row.original.id) : ''}>{editIcon}</Link>
              </ActionIcon>
            )}
            {modelHandelFunction && (
              <ActionIcon color="orange">
                <Button className="bg-transparent" onClick={() => modelHandelFunction(true, row.original)}>
                  {editIcon}
                </Button>
              </ActionIcon>
            )}
            {deleteRow && (
              <ActionIcon color="red">
                {/* place delete confirmation  */}
                <DeleteConfirmation id={row?.original?.id} deleteFunction={deleteRow} />
              </ActionIcon>
            )}
          </Box>
        ),
  })

  return <MantineReactTable table={table} />
}
