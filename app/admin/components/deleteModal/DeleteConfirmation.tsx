'use client'

import toast from 'react-hot-toast'
import { LuTrash2 } from 'react-icons/lu'
import ModalButton from './modal-button'
import { useModal } from './use-modal'

export default function DeleteConfirmation({ id, deleteFunction }: { id: string; deleteFunction: any }) {
  const { closeModal } = useModal()
  const handleDelete = () => {
    deleteFunction(id).then((res: any) => {
      if (res.data.status) {
        toast.success(res.data.message || 'Deleted successfully')
        closeModal()
      }
    })
  }
  return (
    <ModalButton
      view={
        <div>
          <p className="text-center text-lg font-semibold">Are you sure you want to remove this item? </p>
          {/* <p className='text-xs my-5 text-center'></p> */}
          <div className="my-5 flex items-center justify-center space-x-4">
            <button
              className="hover:bg-primary-dark flex items-center space-x-2 rounded-md bg-primary px-3 py-2 text-white transition-all duration-300"
              onClick={closeModal}
            >
              Cancel
            </button>
            <button
              className="-dark flex items-center space-x-2 rounded-md bg-red-500 px-3 py-2 text-white transition-all duration-300 hover:bg-red-400"
              onClick={handleDelete}
            >
              Confirm
            </button>
          </div>
        </div>
      }
      icon={<LuTrash2 color="red" size={20} />}
      label=""
    />
  )
}
