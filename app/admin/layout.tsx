import AuthProvider from '@/features/auth/auth-provider'
import Sidebar from '../components/Layouts/sidebar'
import PageLoading from './components/PageLoading'

function MainLayout({ children }: any) {
  return (
    <AuthProvider>
      <PageLoading>
        <div className={`fixed inset-0 z-50 bg-[black]/60 lg:hidden`}></div>

        <div className={` main-container min-h-screen text-black dark:text-white-dark`}>
          <Sidebar />

          <div
            className={` ml-[220px] transition-all duration-300 ease-in-out`}
            // main-content
          >
            {/* <Header /> */}

            <div className={` animate__animated relative min-h-screen p-1 md:p-2 lg:p-4 xl:p-6`}>
              {children}
              {/* <Footer /> */}
            </div>

            {/* <Portals /> */}
          </div>
        </div>
      </PageLoading>
    </AuthProvider>
  )
}

export default MainLayout
