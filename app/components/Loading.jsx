'use client';

import { useEffect, useState } from 'react';
import { BsQrCodeScan } from 'react-icons/bs';
import { IoQrCodeOutline, IoQrCodeSharp } from 'react-icons/io5';
import { MdOutlineQrCode, MdQrCode } from 'react-icons/md';

const icons = [
  <BsQrCodeScan key='bs' className='animate-ping text-5xl text-blue-500' />,
  <MdOutlineQrCode key='md1' className=' animate-ping text-5xl text-green-500' />,
  <IoQrCodeOutline key='io1' className='animate-ping text-5xl text-purple-500' />,
  <MdOutlineQrCode key='md1' className=' animate-ping text-5xl text-green-500' />,
  <IoQrCodeSharp key='io2' className='animate-ping text-5xl text-red-500' />,
  <MdQrCode key='md3' className=' animate-ping text-5xl text-sky-500' />,
  <MdQrCode key='md5' className='animate-ping text-5xl text-red-500' />,
  <MdQrCode key='md4' className='animate-ping text-5xl text-violet-500' />
];

export default function Loading() {
  const [currentIconIndex, setCurrentIconIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentIconIndex(prevIndex => (prevIndex + 1) % icons.length);
    }, 500);

    return () => clearInterval(interval);
  }, []);

  return (
    <div className='flex flex-col items-center justify-center p-10'>
      <div>{icons[currentIconIndex]}</div>
      <h4 className='mt-10 text-lg font-medium'>Loading . . .</h4>
    </div>
  );
}
