import { useState } from 'react';
import AnimateHeight from 'react-animate-height';
import { FaAngleDown } from 'react-icons/fa6';
import { IoMdClose } from 'react-icons/io';

export default function AccordionSubItem({ title, isActive, array, index, children }) {
    const [active, setActive] = useState(isActive ? '1' : '');
    const togglePara = value => {
        setActive(oldValue => {
            return oldValue === value ? '' : value;
        });
    };
    return (
        <div className='col-span-3 mb-3 font-semibold lg:col-span-2'>
            <div className='rounded border border-gray-100 shadow'>
                <div className='flex items-center justify-between'>
                    <button
                        type='button'
                        className='flex w-full items-center p-4 text-lg font-semibold text-gray-600 dark:bg-[#1b2e4b]'
                        onClick={() => togglePara('1')}
                    >
                        <div className={`ltr:mr-2 rtl:ml-2 ${active === '1' ? 'rotate-180' : ''}`}>
                            <FaAngleDown />
                        </div>
                        {title}
                    </button>
                    <button type='button' onClick={() => array.remove(index)}>
                        <IoMdClose className='mr-3 rounded bg-gray-200 text-3xl text-gray-700 hover:text-red-500' />
                    </button>
                </div>
                <div>
                    <AnimateHeight duration={300} height={active === '1' ? 'auto' : 0}>
                        <div className='space-y-2 border-t border-[#d3d3d3] p-4 text-[13px] text-white-dark dark:border-[#1b2e4b]'>
                            {children}
                        </div>
                    </AnimateHeight>
                </div>
            </div>
        </div>
    );
}
