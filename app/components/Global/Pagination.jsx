import { BsFillArrowLeftSquareFill, BsFillArrowRightSquareFill } from 'react-icons/bs'
import PaginationButton from './PaginationButton'

export default function Pagination({ paginateArray, pageSize, currentPage, setCurrentPage }) {
  const MAX_VISIBLE_PAGES = 10

  function handleNextPage() {
    if (currentPage < Math.ceil(paginateArray.length / pageSize)) {
      setCurrentPage(currentPage + 1)
    }
  }

  function handlePrevPage() {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1)
    }
  }

  const totalPages = Math.ceil(paginateArray?.length / pageSize)

  const pageNumbers = []
  for (let i = 1; i <= totalPages; i++) {
    pageNumbers.push(i)
  }

  function handlePageNumberClick(pageNumber) {
    setCurrentPage(pageNumber)
  }

  let visiblePages
  if (totalPages <= MAX_VISIBLE_PAGES) {
    visiblePages = pageNumbers
  } else {
    const middleIndex = Math.floor(MAX_VISIBLE_PAGES / 2)
    const start = Math.min(Math.max(1, currentPage - middleIndex), totalPages - MAX_VISIBLE_PAGES + 1)
    const end = Math.min(totalPages, start + MAX_VISIBLE_PAGES - 1)

    visiblePages = Array.from({ length: end - start + 1 }, (_, i) => start + i)
  }

  const renderedPages = visiblePages.map((pageNumber, index) => {
    if (index === 0 && pageNumber > 1) {
      return <PaginationButton key={1} pageNumber={1} onClick={() => handlePageNumberClick(1)} />
    }

    if (index === 1 && pageNumber > 2) {
      return <PaginationButton key={`ellipsis-start`} isEllipsis />
    }

    if (index === visiblePages.length - 1 && pageNumber < totalPages) {
      return <PaginationButton key={totalPages} pageNumber={totalPages} onClick={() => handlePageNumberClick(totalPages)} />
    }

    if (index === visiblePages.length - 2 && pageNumber < totalPages - 1) {
      return <PaginationButton key={`ellipsis-end`} isEllipsis />
    }

    return (
      <PaginationButton
        key={pageNumber}
        pageNumber={pageNumber}
        onClick={() => handlePageNumberClick(pageNumber)}
        isActive={pageNumber === currentPage}
      />
    )
  })

  return (
    <>
      {paginateArray?.length > pageSize && (
        <div className="my-4 flex justify-center gap-2">
          <button onClick={handlePrevPage} disabled={currentPage === 1}>
            <BsFillArrowLeftSquareFill
              className={`rounded text-3xl font-semibold ${currentPage === 1 ? 'text-gray-300' : 'text-blue-500 hover:text-blue-600'}`}
            />
          </button>

          {renderedPages}

          <button onClick={handleNextPage} disabled={currentPage === totalPages}>
            <BsFillArrowRightSquareFill
              className={`rounded text-3xl font-semibold ${currentPage === totalPages ? 'text-gray-300' : 'text-blue-500 hover:text-blue-600'}`}
            />
          </button>
        </div>
      )}
    </>
  )
}
