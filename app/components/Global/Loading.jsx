'use client';

import { useEffect, useState } from 'react';
import { FcSportsMode } from 'react-icons/fc';
import { GiAmericanFootballPlayer } from 'react-icons/gi';
import { MdSportsGymnastics, MdSportsHandball, MdSportsMartialArts } from 'react-icons/md';

const icons = [
  <MdSportsGymnastics key='icon1' className='animate-ping text-5xl text-blue-500' />,
  <MdSportsHandball key='icon2' className=' animate-ping text-5xl text-green-500' />,
  <MdSportsMartialArts key='icon3' className='animate-ping text-5xl text-purple-500' />,
  <MdSportsHandball key='icon4' className=' animate-ping text-5xl text-green-500' />,
  <FcSportsMode key='icon5' className='animate-ping text-5xl text-red-500' />,
  <GiAmericanFootballPlayer key='icon6' className=' animate-ping text-2xl text-sky-500' />,
  <GiAmericanFootballPlayer key='icon7' className='animate-ping text-3xl text-red-500' />,
  <GiAmericanFootballPlayer key='icon8' className='animate-ping text-4xl text-violet-500' />,
  <GiAmericanFootballPlayer key='icon9' className='animate-ping text-5xl text-violet-600' />,
  <GiAmericanFootballPlayer key='icon10' className='animate-ping text-6xl text-violet-700' />,
  <GiAmericanFootballPlayer key='icon11' className='animate-ping text-7xl text-violet-800' />,
  <GiAmericanFootballPlayer key='icon12' className='animate-ping text-8xl text-violet-900' />,
  <GiAmericanFootballPlayer key='icon13' className='animate-ping text-9xl text-violet-900' />,
  <GiAmericanFootballPlayer key='icon13' className='animate-ping text-[200px] text-violet-900' />,
  <GiAmericanFootballPlayer key='icon13' className='animate-ping text-[500px] text-violet-900' />,
  <GiAmericanFootballPlayer key='icon13' className='animate-ping text-[800px] text-red-600' />
];

export default function Loading() {
  const [currentIconIndex, setCurrentIconIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentIconIndex(prevIndex => (prevIndex + 1) % icons.length);
    }, 500);

    return () => clearInterval(interval);
  }, []);

  return (
    <div className='flex flex-col items-center justify-center p-10'>
      <div className='min-h-16'>{icons[currentIconIndex]}</div>
      <h4 className='mt-10 animate-pulse text-lg font-medium text-gray-500'>Loading . . .</h4>
    </div>
  );
}
