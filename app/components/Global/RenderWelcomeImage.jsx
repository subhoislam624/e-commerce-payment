export default function RenderWelcomeImage({ welcomeImage, animationDuration }) {
    return (
        <div className='relative flex h-full w-full items-center' style={{ animation: `slideInFromBottom ${animationDuration}s ease-out` }}>
            <div className='flex items-center justify-center'>
                <img className='absolute left-0 right-0 top-16 mx-auto' src={welcomeImage} width={200} height={200} alt='Welcome Image' />
            </div>
        </div>
    );
}
