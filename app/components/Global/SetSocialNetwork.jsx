import { Field, FieldArray } from 'formik'
import Image from 'next/image'
import SocialNetworkEntry from './SocialNetworkEntry'

const socialNetworks = [
  { name: 'Dribbble', icon: 'dribbble.svg' },
  { name: 'Facebook', icon: 'facebook.svg' },
  { name: 'YouTube', icon: 'youtube.svg' },
  { name: 'Instagram', icon: 'instagram.svg' },
  { name: 'Flickr', icon: 'flickr.svg' },
  { name: 'GitHub', icon: 'github.svg' },
  { name: 'Google', icon: 'google.svg' },
  { name: 'Line', icon: 'line.svg' },
  { name: 'LinkedIn', icon: 'linkedin.svg' },
  { name: 'Pinterest', icon: 'pinterest.svg' },
  { name: 'Reddit', icon: 'reddit.svg' },
  { name: 'Skype', icon: 'skype.svg' },
  { name: 'Snapchat', icon: 'snapchat.svg' },
  { name: 'TripAdvisor', icon: 'tripadvisor.svg' },
  { name: 'Tumblr', icon: 'tumblr.svg' },
  { name: 'Twitter', icon: 'twitter.svg' },
  { name: 'Vimeo', icon: 'vimeo.svg' },
  { name: 'Vkontakte', icon: 'vkontakte.svg' },
  { name: 'Web', icon: 'web.svg' },
  { name: 'Xing', icon: 'xing.svg' },
  { name: 'X', icon: 'x.svg' },
  { name: 'TikTok', icon: 'tiktok.svg' },
  { name: 'WhatsApp', icon: 'whatsapp.svg' },
  { name: 'Telegram', icon: 'telegram.svg' },
  { name: 'Messenger', icon: 'messenger.svg' },
  { name: 'Yelp', icon: 'yelp.svg' },
  { name: 'Postmates', icon: 'postmates.svg' },
  { name: 'OpenTable', icon: 'opentable.svg' },
  { name: 'Spotify', icon: 'spotify.svg' },
  { name: 'SoundCloud', icon: 'soundcloud.svg' },
  { name: 'Apple Music', icon: 'applemusic.svg' },
  { name: 'Signal', icon: 'signal.svg' },
]

const SetSocialNetwork = ({ values }) => {
  return (
    <div>
      <div>
        <label className="mt-2 text-[14px] font-semibold text-gray-800">Title</label>
        <Field className="w-full rounded border border-gray-300 px-4 py-2" name="qr_type_info.social_networks.title" placeholder="Title" />
      </div>
      <div className="mt-3">
        <FieldArray name="qr_type_info.social_networks.networks">
          {(arrayHelpers) => (
            <div>
              {values.qr_type_info.social_networks.networks.map((network, index) => (
                <SocialNetworkEntry key={index} index={index} arrayHelpers={arrayHelpers} network={network} values={values} />
              ))}
              <h4 className="py-3 text-lg font-semibold text-gray-800">Add Social Media</h4>
              <div className="flex flex-wrap gap-3 pt-3">
                {socialNetworks.map((network) => (
                  <div key={network.name} className="tooltip text-sm" data-tip={network.name}>
                    <button
                      className="mb-2"
                      type="button"
                      onClick={() => arrayHelpers.push({ name: network.name, icon: network.icon, url: '', text: '' })}
                    >
                      <div className="w-full">
                        <Image
                          src={`/svg/icons/${network.icon}`}
                          alt="social icons"
                          className="lg:max-w-[370px] xl:max-w-[500px]"
                          width={30}
                          height={30}
                          sizes="100vw"
                        />
                      </div>
                    </button>
                  </div>
                ))}
              </div>
            </div>
          )}
        </FieldArray>
      </div>
    </div>
  )
}

export default SetSocialNetwork
