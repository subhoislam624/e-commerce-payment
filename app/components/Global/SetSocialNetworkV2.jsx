import { Field, FieldArray } from 'formik'
import { FaDribbble, FaFacebook, FaFlickr } from 'react-icons/fa'

const networkIcons = [
  { name: 'dribbble', icon: FaDribbble },
  { name: 'facebook', icon: FaFacebook },
  { name: 'flickr', icon: FaFlickr },
  // ... Add more network icons as needed
]

const SocialNetworkItem = ({ index, arrayHelpers }) => (
  <div key={index}>
    <h4 className="px-2 text-xl font-semibold capitalize">{networkIcons[index].name}</h4>
    <Field
      className="w-full rounded border border-gray-300 px-4 py-2"
      name={`qr_type_info.social_networks.networks[${index}].icon`}
      placeholder="Icon"
    />
    <Field
      className="w-full rounded border border-gray-300 px-4 py-2"
      name={`qr_type_info.social_networks.networks[${index}].url`}
      placeholder="URL"
    />
    <Field
      className="w-full rounded border border-gray-300 px-4 py-2"
      name={`qr_type_info.social_networks.networks[${index}].text`}
      placeholder="Text"
    />
    <button className="btn btn-error btn-sm my-2" type="button" onClick={() => arrayHelpers.remove(index)}>
      Remove
    </button>
  </div>
)

export default function SetSocialNetworkV2({ values }) {
  return (
    <div>
      <div>
        <label className="mt-2 text-[14px] font-semibold text-gray-800">Title</label>
        <Field className="w-full rounded border border-gray-300 px-4 py-2" name="qr_type_info.social_networks.title" placeholder="Title" />
      </div>
      <div>
        <label className="mt-2 text-[14px] font-semibold text-gray-800">Networks</label>
        <FieldArray name="qr_type_info.social_networks.networks">
          {(arrayHelpers) => (
            <div>
              {values.qr_type_info.social_networks.networks.map((network, index) => (
                <SocialNetworkItem key={index} index={index} arrayHelpers={arrayHelpers} />
              ))}

              <div className="flex flex-wrap gap-2">
                {networkIcons.map(({ name, icon: Icon }) => (
                  <button
                    key={name}
                    className="btn btn-primary btn-sm"
                    type="button"
                    onClick={() => arrayHelpers.push({ name, icon: '', url: '', text: '' })}
                  >
                    {name}
                  </button>
                ))}
              </div>
            </div>
          )}
        </FieldArray>
      </div>
    </div>
  )
}
