"use client";
import { BiFootball } from "react-icons/bi";

export default function GlobalLoading() {
  return (
    <div className="mt-5 flex items-center justify-center p-2 md:mt-10">
      <div className="flex items-center">
        <div className="animate-bounce [animation-delay:-0.3s]">
          <BiFootball className="animate-spin  text-3xl " />
        </div>
        <div className="animate-bounce [animation-delay:-0.15s]">
          <BiFootball className="animate-spin text-3xl text-secondary " />
        </div>
        <div className="animate-bounce ">
          <BiFootball className="animate-spin  text-3xl" />
        </div>
      </div>
    </div>
  );
}
