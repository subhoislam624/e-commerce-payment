import Link from 'next/link'
import { FaHome } from 'react-icons/fa'
import { FaAngleRight } from 'react-icons/fa6'

export default function BreadCrumb({ breadMenu }) {
  return (
    <nav className="mb-3 flex">
      <Link href="/admin/dashboard">
        <FaHome className="mr-2 text-xl" />
      </Link>

      <ol className="inline-flex items-center space-x-1 md:space-x-3">
        {breadMenu?.map((item, index) => (
          <li key={index} className="inline-flex items-center">
            <FaAngleRight className="mr-2 text-sm" />

            <Link
              href={item.link || '#'}
              className="mr-2 inline-flex cursor-pointer items-center text-base font-bold text-gray-900 hover:text-gray-700 dark:text-gray-400 dark:hover:text-white"
            >
              <span className="capitalize">{item.path}</span>
            </Link>

            {index === breadMenu.length - 1 && (
              <svg className="mx-1 h-3 w-3 text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 9 4-4-4-4" />
              </svg>
            )}
          </li>
        ))}
      </ol>
    </nav>
  )
}
