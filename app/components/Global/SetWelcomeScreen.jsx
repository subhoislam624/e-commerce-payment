import { Field } from "formik";
import Image from "next/image";
import { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { BsImage } from "react-icons/bs";
import { HiPlus } from "react-icons/hi";

export default function SetWelcomeScreen({
  values,
  setFieldValue,
  welcomeImg,
  setWelcomeImg,
}) {
  const onDrop = useCallback(
    (acceptedFiles) => {
      if (acceptedFiles?.length) {
        const selectedFile = acceptedFiles[0];
        const fileWithPreview = Object.assign(selectedFile, {
          preview: URL.createObjectURL(selectedFile),
        });
        setWelcomeImg(fileWithPreview);
        setFieldValue("qr_type_info.welcome_img", fileWithPreview.preview);
      }
      // DSWTF
    },
    [setFieldValue, setWelcomeImg]
  );

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: {
      "image/*": [".jpeg", ".jpg", ".png"],
    },
    maxFiles: 1,
    maxSize: 1024 * 1000,
  });

  const removeFile = (name) => {
    setWelcomeImg(null);
    setFieldValue("qr_type_info.welcome_img", null);
  };

  return (
    <div>
      <h4 className="mb-2 text-base text-gray-700">Image</h4>
      <div className="flex items-center justify-start gap-10">
        <div {...getRootProps()}>
          <input {...getInputProps()} />

          {welcomeImg ? (
            <div>
              <Image src={welcomeImg.preview} alt="" width={100} height={100} />
            </div>
          ) : (
            <div className="relative mt-1 w-fit cursor-pointer rounded-xl border border-gray-200 p-0.5 px-2">
              <BsImage className="text-7xl" />
              <HiPlus className="absolute -right-2 -top-2 rounded-full border border-gray-100 bg-gray-800 text-3xl text-gray-50" />
            </div>
          )}
        </div>

        {welcomeImg && (
          <button
            type="button"
            className="btn btn-error btn-sm rounded-full text-white"
            onClick={() => removeFile(welcomeImg.name)}
          >
            Delete
          </button>
        )}
      </div>
      <div className="mt-3">
        <h4 className="mb-2 text-base text-gray-700">Duration</h4>
        <div
          className="tooltip tooltip-info tooltip-right"
          data-tip={values.qr_type_info.welcome_duration}
        >
          <Field
            type="range"
            className="transparent h-1.5 w-[300px] cursor-pointer appearance-none rounded-lg border-transparent bg-neutral-200"
            name="qr_type_info.welcome_duration"
            step="1"
            min="1"
            max="10"
          />
        </div>
      </div>
    </div>
  );
}
