import { Field } from 'formik'
import Image from 'next/image'
import { IoMdClose } from 'react-icons/io'

const SocialNetworkEntry = ({ index, arrayHelpers, values }) => {
  return (
    <div className="py-5">
      <div className="mb-3 flex items-center justify-start gap-2">
        <Image
          src={`/svg/icons/${values.qr_type_info.social_networks.networks[index].icon}`}
          alt="social icon"
          className="lg:max-w-[370px] xl:max-w-[500px]"
          width={40}
          height={40}
          sizes="100vw"
        />

        <h4 className="px-2 text-lg font-semibold capitalize text-gray-600">{values.qr_type_info.social_networks.networks[index].name}</h4>
      </div>
      <div className="flex items-center gap-2">
        <Field
          className="w-full rounded border border-gray-300 px-4 py-2"
          name={`qr_type_info.social_networks.networks[${index}].url`}
          placeholder="URL"
        />
        <Field
          className="w-full rounded border border-gray-300 px-4 py-2"
          name={`qr_type_info.social_networks.networks[${index}].text`}
          placeholder="Text"
        />
        <button type="button" className="rounded bg-gray-200 text-2xl text-gray-700 hover:text-red-500" onClick={() => arrayHelpers.remove(index)}>
          <IoMdClose className="" />
        </button>
      </div>
    </div>
  )
}

export default SocialNetworkEntry
