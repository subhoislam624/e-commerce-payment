import { ErrorMessage, Field, FieldArray } from 'formik'
import { IoMdAdd, IoMdClose } from 'react-icons/io'

const SetOpeningHours = ({ values, handleChange, handleBlur }) => {
  const is24HourFormat = values.qr_type_info.opening_hours.format === '24'

  return (
    <div>
      <div>
        <div className="my-2 flex space-x-2">
          <button
            type="button"
            className={`rounded px-4 py-2 ${
              values.qr_type_info.opening_hours.format === '12' ? 'bg-blue-500 text-white' : 'bg-gray-300 text-gray-800'
            }`}
            onClick={() => handleChange('qr_type_info.opening_hours.format')('12')}
          >
            12-hour format
          </button>
          <button
            type="button"
            className={`rounded px-4 py-2 ${
              values.qr_type_info.opening_hours.format === '24' ? 'bg-blue-500 text-white' : 'bg-gray-300 text-gray-800'
            }`}
            onClick={() => handleChange('qr_type_info.opening_hours.format')('24')}
          >
            24-hour format
          </button>
        </div>
        <ErrorMessage name="qr_type_info.opening_hours.format" component="div" className="error-message" />
      </div>

      <FieldArray name="qr_type_info.opening_hours.hours_schedule">
        {() => (
          <div className="divide-y divide-gray-400">
            {values.qr_type_info.opening_hours.hours_schedule.map((day, dayIndex) => (
              <div key={dayIndex} className="grid grid-cols-12 justify-start ">
                <div className="col-span-3 mt-3 flex h-[45px] items-start gap-2">
                  <Field
                    type="checkbox"
                    className="checkbox checkbox-sm"
                    name={`qr_type_info.opening_hours.hours_schedule[${dayIndex}].is_active`}
                    checked={day.is_active}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <h4 className={`text-[14px] font-semibold ${day.is_active ? ' text-gray-800' : 'text-gray-400'}`}>
                    {day.day.charAt(0).toUpperCase() + day.day.slice(1)}
                  </h4>
                </div>

                {day.is_active ? (
                  <div className="col-span-9 text-gray-800">
                    <FieldArray name={`qr_type_info.opening_hours.hours_schedule[${dayIndex}].hours`}>
                      {({ push: pushHours, remove: removeHours }) => (
                        <div className="flex items-start gap-2">
                          <div>
                            {day.hours.map((hours, hoursIndex) => (
                              <div key={hoursIndex} className="my-2 flex items-center justify-start gap-2">
                                <div>
                                  <Field
                                    className="w-full rounded border border-gray-300 px-4 py-2"
                                    type="time"
                                    name={`qr_type_info.opening_hours.hours_schedule[${dayIndex}].hours[${hoursIndex}].start`}
                                    value={hours.start}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    format={is24HourFormat ? 'HH:mm' : 'hh:mm a'}
                                  />
                                  <ErrorMessage
                                    name={`qr_type_info.opening_hours.hours_schedule[${dayIndex}].hours[${hoursIndex}].start`}
                                    component="div"
                                    className="error-message"
                                  />
                                </div>
                                <div>
                                  <Field
                                    className="w-full rounded border border-gray-300 px-4 py-2"
                                    type="time"
                                    name={`qr_type_info.opening_hours.hours_schedule[${dayIndex}].hours[${hoursIndex}].end`}
                                    value={hours.end}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    format={is24HourFormat ? 'HH:mm' : 'hh:mm a'}
                                  />
                                  <ErrorMessage
                                    name={`qr_type_info.opening_hours.hours_schedule[${dayIndex}].hours[${hoursIndex}].end`}
                                    component="div"
                                    className="error-message"
                                  />
                                </div>
                                {hoursIndex > 0 && (
                                  <button className="" type="button" onClick={() => removeHours(hoursIndex)}>
                                    <IoMdClose className="rounded bg-gray-200 text-2xl text-gray-700 hover:text-red-500" />
                                  </button>
                                )}
                                {hoursIndex === 0 && (
                                  <button className="" type="button" onClick={() => pushHours({ start: '08:00', end: '20:00' })}>
                                    <IoMdAdd className="rounded bg-gray-200 text-2xl text-green-600 hover:text-green-500" />
                                  </button>
                                )}
                              </div>
                            ))}
                          </div>
                        </div>
                      )}
                    </FieldArray>
                  </div>
                ) : (
                  <>
                    <div className="col-span-9 text-gray-400">
                      <div className="my-2 flex items-center justify-start gap-2 blur-[1px]">
                        <div>
                          <Field disabled className="w-full rounded border border-gray-300 px-4 py-2" type="time" />
                        </div>
                        <div>
                          <Field disabled className="w-full rounded border border-gray-300 px-4 py-2" type="time" />
                        </div>

                        <button className="" type="button" disabled>
                          <IoMdAdd className="rounded bg-gray-200 text-2xl text-gray-400" />
                        </button>
                      </div>
                    </div>
                  </>
                )}
              </div>
            ))}
          </div>
        )}
      </FieldArray>
    </div>
  )
}

export default SetOpeningHours
