import { useEffect, useRef, useState } from 'react';

const Dropdown = () => {
    const [isOpen, setIsOpen] = useState(false);
    const dropdownRef = useRef(null);

    // Use useEffect to add a click event listener to the document
    useEffect(() => {
        const handleClickOutside = e => {
            if (dropdownRef.current && !dropdownRef.current.contains(e.target)) {
                setIsOpen(false);
            }
        };

        document.addEventListener('click', handleClickOutside);

        // Clean up the event listener on unmount
        return () => {
            document.removeEventListener('click', handleClickOutside);
        };
    }, []);

    return (
        <div className='dropdown' ref={dropdownRef}>
            <button onClick={() => setIsOpen(!isOpen)}>Open Dropdown</button>
            {isOpen && (
                <ul className='dropdown-list'>
                    <li>Menu 1</li>
                    <li>Menu 2</li>
                    <li>Menu 3</li>
                </ul>
            )}
        </div>
    );
};

export default Dropdown;
