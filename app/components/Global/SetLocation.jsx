import { Field } from 'formik'
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'

export default function SetLocation() {
  return (
    <Tabs>
      <TabList>
        <Tab>Complete Location</Tab>
        <Tab>URL</Tab>
        <Tab>Coordinates</Tab>
      </TabList>

      <TabPanel>
        <div className="grid grid-cols-2 gap-3">
          <div>
            <label className="mt-2 text-[14px] font-semibold text-gray-800">Street</label>
            <Field className="w-full rounded border border-gray-300 px-4 py-2" name="qr_type_info.location.complete.street" placeholder="Street" />
          </div>
          <div>
            <label className="mt-2 text-[14px] font-semibold text-gray-800">Number</label>
            <Field className="w-full rounded border border-gray-300 px-4 py-2" name="qr_type_info.location.complete.number" placeholder="Number" />
          </div>
          <div>
            <label className="mt-2 text-[14px] font-semibold text-gray-800">Postal Code</label>
            <Field
              className="w-full rounded border border-gray-300 px-4 py-2"
              name="qr_type_info.location.complete.postal_code"
              placeholder="Postal Code"
            />
          </div>
          <div>
            <label className="mt-2 text-[14px] font-semibold text-gray-800">City</label>
            <Field className="w-full rounded border border-gray-300 px-4 py-2" name="qr_type_info.location.complete.city" placeholder="City" />
          </div>
          <div>
            <label className="mt-2 text-[14px] font-semibold text-gray-800">State</label>
            <Field className="w-full rounded border border-gray-300 px-4 py-2" name="qr_type_info.location.complete.state" placeholder="State" />
          </div>
          <div>
            <label className="mt-2 text-[14px] font-semibold text-gray-800">Country</label>
            <Field className="w-full rounded border border-gray-300 px-4 py-2" name="qr_type_info.location.complete.country" placeholder="Country" />
          </div>
        </div>
      </TabPanel>

      <TabPanel>
        <div>
          <label className="mt-2 text-[14px] font-semibold text-gray-800">URL</label>
          <Field className="w-full rounded border border-gray-300 px-4 py-2" name="qr_type_info.location.url" placeholder="URL" />
        </div>
      </TabPanel>

      <TabPanel>
        <div className="grid grid-cols-2 gap-3">
          <div>
            <label className="mt-2 text-[14px] font-semibold text-gray-800">Latitude</label>
            <Field
              className="w-full rounded border border-gray-300 px-4 py-2"
              name="qr_type_info.location.coordinates.latitude"
              placeholder="Latitude"
            />
          </div>
          <div>
            <label className="mt-2 text-[14px] font-semibold text-gray-800">Longitude</label>
            <Field
              className="w-full rounded border border-gray-300 px-4 py-2"
              name="qr_type_info.location.coordinates.longitude"
              placeholder="Longitude"
            />
          </div>
        </div>
      </TabPanel>
    </Tabs>
  )
}
