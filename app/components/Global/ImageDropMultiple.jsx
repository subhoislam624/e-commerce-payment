import Image from 'next/image'
import { useCallback, useEffect, useState } from 'react'
import { useDropzone } from 'react-dropzone'
import { FaTrashAlt } from 'react-icons/fa'

const ImageDropMultiple = ({ className, values, setFieldValue, files, setFiles }) => {
  const [newPreviews, setNewPreviews] = useState([])

  const onDrop = useCallback(
    (acceptedFiles) => {
      if (acceptedFiles?.length) {
        const newFiles = acceptedFiles.map((file) => Object.assign(file, { preview: URL.createObjectURL(file) }))
        setFiles((previousFiles) => [...previousFiles, ...newFiles])
        const previews = [...newPreviews, ...newFiles.map((file) => file.preview)]
        setNewPreviews(previews)
        setFieldValue('qr_type_info.images', previews)
      }
    },
    [setFieldValue, newPreviews, setFiles],
  )

  const { getRootProps, getInputProps } = useDropzone({
    accept: {
      'image/*': ['.jpeg', '.jpg', '.png'],
    },
    maxSize: 1024 * 1000,
    onDrop,
  })

  useEffect(() => {
    // Revoke the data uris to avoid memory leaks
    return () => files.forEach((file) => URL.revokeObjectURL(file.preview))
  }, [files])

  const removeFile = (preview) => {
    setFiles((previousFiles) => previousFiles.filter((file) => file.preview !== preview))
    const updatedPreviews = newPreviews.filter((preview) => preview !== preview)
    setNewPreviews(updatedPreviews)
    setFieldValue(
      'qr_type_info.images',
      values.qr_type_info.images.filter((image) => image !== preview),
    )
  }

  const removeAll = () => {
    setFiles([])
    setFieldValue('qr_type_info.images', '')
  }

  return (
    <>
      <div
        {...getRootProps({
          className: className,
        })}
      >
        <input {...getInputProps()} />
        <div className="flex flex-col items-center justify-center gap-4 rounded-lg border-2 border-dashed p-5">
          <div className="btn btn-primary ">Upload Images</div>
          <p>Maximum Size: 1MB</p>
        </div>
      </div>

      {/* Preview */}
      <section className="mt-3">
        <div className="flex gap-4">
          <button
            type="button"
            onClick={removeAll}
            className="border-secondary-400 hover:bg-secondary-400 mt-1 rounded border px-3 text-[12px] font-bold uppercase tracking-wider text-neutral-500 transition-colors hover:bg-green-500"
          >
            Remove all
          </button>
        </div>

        <ul className="my-3 grid grid-cols-1 gap-3 ">
          {files.map((file) => (
            <li key={file.name} className="flex items-center justify-between rounded-lg border border-gray-200 p-2">
              <div className="flex items-center gap-3">
                <Image
                  src={file.preview}
                  alt={file.name}
                  width={100}
                  height={100}
                  onLoad={() => {
                    URL.revokeObjectURL(file.preview)
                  }}
                  className="w-2h-24 h-24 rounded border border-gray-200 object-contain p-1"
                />
                <p className="mt-2 text-[14px] font-bold text-gray-800">{file.name}</p>
              </div>
              <button type="button" className="rounded bg-red-500 p-1" onClick={() => removeFile(file.preview)}>
                <FaTrashAlt className="hover:fill-secondary-400 h-5 w-5 fill-white transition-colors" />
              </button>
            </li>
          ))}
        </ul>
      </section>
    </>
  )
}

export default ImageDropMultiple
