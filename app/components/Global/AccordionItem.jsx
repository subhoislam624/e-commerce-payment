import { useState } from 'react'
import AnimateHeight from 'react-animate-height'
import { FaAngleDown } from 'react-icons/fa6'
import { IoMdFootball } from 'react-icons/io'

export default function AccordionItem({ title, isActive, children, icon }) {
  const [active, setActive] = useState(isActive ? '1' : '')
  const togglePara = (value) => {
    setActive((oldValue) => {
      return oldValue === value ? '' : value
    })
  }
  return (
    <div className="col-span-3 mb-3 font-semibold lg:col-span-2">
      <div className="rounded border border-[#d3d3d3] bg-white dark:border-[#1b2e4b]">
        <button type="button" className="flex w-full items-center p-4 text-xl font-bold dark:bg-[#1b2e4b]" onClick={() => togglePara('1')}>
          {icon || <IoMdFootball className="mr-2 text-2xl text-primary" />} {title}
          <div className={`ltr:ml-auto rtl:mr-auto ${active === '1' ? 'rotate-180' : ''}`}>
            <FaAngleDown />
          </div>
        </button>
        <div>
          <AnimateHeight duration={300} height={active === '1' ? 'auto' : 0}>
            <div className="space-y-2 border-t border-[#d3d3d3] p-4 text-[13px] text-white-dark dark:border-[#1b2e4b]">{children}</div>
          </AnimateHeight>
        </div>
      </div>
    </div>
  )
}
