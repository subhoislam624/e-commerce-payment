import { useState } from "react";
import AnimateHeight from "react-animate-height";
import { FaAngleDown } from "react-icons/fa6";
import { IoMdClose } from "react-icons/io";

export default function AccordionDetailsItem({
  ad,
  isActive,
  array,
  index,
  children,
}) {
  const [active, setActive] = useState(isActive ? "1" : "");
  const [clickTimer, setClickTimer] = useState(null);

  const togglePara = (value) => {
    setActive((oldValue) => {
      return oldValue === value ? "" : value;
    });
  };

  const handleDoubleClick = () => {
    array.remove(index);
  };

  const handleClick = () => {
    // Set a timer to distinguish between single-click and double-click
    if (!clickTimer) {
      setClickTimer(
        setTimeout(() => {
          // Handle single-click action here
          setClickTimer(null);
        }, 300)
      ); // Adjust the time interval as needed
    } else {
      // Clear the timer on double-click and trigger the removal
      clearTimeout(clickTimer);
      setClickTimer(null);
      handleDoubleClick();
    }
  };

  return (
    <div className="col-span-3 mb-3 font-semibold lg:col-span-2">
      <div className="rounded border border-gray-100 shadow">
        <div className="flex items-center justify-between">
          <button
            type="button"
            className="flex w-full items-center p-4 text-lg font-semibold text-gray-600 dark:bg-[#1b2e4b]"
            onClick={() => togglePara("1")}
          >
            <div
              className={`ltr:mr-2 rtl:ml-2 ${
                active === "1" ? "rotate-180" : ""
              }`}
            >
              <FaAngleDown />
            </div>

            <div className="grid w-full grid-cols-3 gap-4 px-10">
              <h4 className="text-start">{ad.name}</h4>

              <p className="capitalize">{ad.ad_type}</p>

              <span
                className={`mx-auto w-fit caption-top rounded-full px-4 py-1 text-sm capitalize text-white ${
                  ad.status === "1" ? "bg-green-400" : "bg-red-400"
                } `}
              >
                {ad.status === "1" ? "active" : "in-active"}
              </span>
            </div>
          </button>
          <div
            className="tooltip tooltip-error tooltip-left flex items-center justify-center"
            data-tip="Double-click to Delete"
          >
            <button type="button" onClick={handleClick}>
              <IoMdClose className="mr-3 rounded bg-gray-200 text-3xl text-gray-700 hover:text-red-500" />
            </button>
          </div>
        </div>
        <div>
          <AnimateHeight duration={300} height={active === "1" ? "auto" : 0}>
            <div className="space-y-2 border-t border-[#d3d3d3] p-4 text-[13px] text-white-dark dark:border-[#1b2e4b]">
              {children}
            </div>
          </AnimateHeight>
        </div>
      </div>
    </div>
  );
}
