// PaginationButton.js

function PaginationButton({ pageNumber, onClick, isActive, isEllipsis }) {
  const classNames = `h-8 min-w-[32px] w-fit rounded hover:bg-blue-500 hover:text-white ${
    isActive ? 'bg-blue-500 text-white' : 'border border-blue-300 bg-white text-blue-500'
  }`

  return isEllipsis ? (
    <span className="mx-2">...</span>
  ) : (
    <button onClick={onClick} className={classNames}>
      {pageNumber}
    </button>
  )
}

export default PaginationButton
