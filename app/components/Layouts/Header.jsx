'use client'

import Image from 'next/image'
import { FiLogOut } from 'react-icons/fi'
import Dropdown from '../Dropdown'
import GlobalModal from '../Modal/GlobalModal'

const Header = () => {
  // const router = useRouter()

  // const [user, setUser] = useState({})
  // const [profileModal, setProfileModal] = useState(false)
  // const [changePasswordModal, setChangePasswordModal] = useState(false)

  // useEffect(() => {
  //   const selector = document.querySelector('ul.horizontal-menu a[href="' + window.location.pathname + '"]')
  //   if (selector) {
  //     const all = document.querySelectorAll('ul.horizontal-menu .nav-link.active')
  //     for (let i = 0; i < all.length; i++) {
  //       all[0]?.classList.remove('active')
  //     }

  //     let allLinks = document.querySelectorAll('ul.horizontal-menu a.active')
  //     for (let i = 0; i < allLinks.length; i++) {
  //       const element = allLinks[i]
  //       element?.classList.remove('active')
  //     }
  //     selector?.classList.add('active')

  //     const ul = selector.closest('ul.sub-menu')
  //     if (ul) {
  //       let ele = ul.closest('li.menu').querySelectorAll('.nav-link')
  //       if (ele) {
  //         ele = ele[0]
  //         setTimeout(() => {
  //           ele?.classList.add('active')
  //         })
  //       }
  //     }
  //   }
  // }, [router.pathname])

  // const isRtl = useSelector((state) => state.themeConfig.rtlClass) === 'rtl' ? true : false

  // const themeConfig = useSelector((state) => state.themeConfig)
  // const setLocale = (flag) => {
  //   setFlag(flag)
  //   if (flag.toLowerCase() === 'ae') {
  //     dispatch(toggleRTL('rtl'))
  //   } else {
  //     dispatch(toggleRTL('ltr'))
  //   }
  // }
  // const [flag, setFlag] = useState('')

  // const dispatch = useDispatch()

  // const handleSignOut = async () => {
  //   signOut({
  //     callbackUrl: '/admin-login',
  //   })
  // }

  return (
    <header>
      <div className="shadow-sm">
        <div className="relative flex w-full items-center bg-white px-5 py-2.5 dark:bg-black">
          {/* <div className="horizontal-logo flex items-center justify-between lg:hidden ltr:mr-2">
            <Link href="/admin/dashboard" className="main-logo flex shrink-0 items-center">
              <img className="mx-auto h-10" src="/logo.png" alt="App logo" />
              
              <h1 className="text-3xl font-extrabold">E-commerce</h1>
            </Link>
            <button
              type="button"
              className="collapse-icon flex flex-none rounded-full bg-white-light/40 p-2 hover:bg-white-light/90 hover:text-primary dark:bg-dark/40 dark:text-[#d0d2d6] dark:hover:bg-dark/60 dark:hover:text-primary lg:hidden ltr:ml-2 rtl:mr-2"
              onClick={() => dispatch(toggleSidebar())}
            >
              <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M20 7L4 7" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" />
                <path opacity="0.5" d="M20 12L4 12" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" />
                <path d="M20 17L4 17" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" />
              </svg>
            </button>
          </div> */}

          <div className="ml-auto flex w-full items-center justify-between space-x-1.5 dark:text-[#d0d2d6] sm:flex-1 lg:space-x-2">
            <div className="sm:ltr:mr-auto">
              {/* <h4 className="ml-2 rounded border border-gray-200 p-2 text-base">
                Welcome Back
                <span className="ml-2 font-bold text-primary">{user?.name}</span>
              </h4>

              <button
                type="button"
                onClick={() => setSearch(!search)}
                className="search_btn rounded-full bg-white-light/40 p-2 hover:bg-white-light/90 dark:bg-dark/40 dark:hover:bg-dark/60 sm:hidden"
              >
                <svg
                  className="mx-auto h-4.5 w-4.5 dark:text-[#d0d2d6]"
                  width="20"
                  height="20"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <circle cx="11.5" cy="11.5" r="9.5" stroke="currentColor" strokeWidth="1.5" opacity="0.5" />
                  <path d="M18.5 18.5L22 22" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" />
                </svg>
              </button> */}
            </div>

            {false && (
              <div className="dropdown flex shrink-0">
                <Dropdown
                  offset={[0, 8]}
                  placement={`${isRtl ? 'bottom-start' : 'bottom-end'}`}
                  btnClassName="relative group block"
                  button={
                    user.image ? (
                      <Image className="h-9 w-9 rounded-full object-cover" src={user.image} alt="user-logo" width={0} height={0} sizes="100vw" />
                    ) : (
                      <Image
                        className="h-9 w-9 rounded-full object-cover"
                        src="/default/user.png"
                        alt="user-logo"
                        width={0}
                        height={0}
                        sizes="100vw"
                      />
                    )
                  }
                >
                  <ul className="w-[230px] !bg-gray-200 !py-0 font-semibold text-dark shadow dark:text-white-dark dark:text-white-light/90">
                    {/* <li>
                      <div className="flex items-center px-4 py-4">
                        <Image
                          className="h-10 w-10 rounded object-cover"
                          src="/default/user.png"
                          alt="userProfile"
                          width={0}
                          height={0}
                          sizes="100vw"
                        />
                        <div className="truncate ltr:pl-4 rtl:pr-4">
                          <h4 className="text-base capitalize">
                            {user.name}
                            <span className="rounded bg-success-light px-1 text-xs text-success ltr:ml-2 rtl:ml-2">Pro</span>
                          </h4>
                          <button type="button" className="text-black/60 hover:text-primary dark:text-dark-light/60 dark:hover:text-white">
                            {user.email}
                          </button>
                        </div>
                      </div>
                    </li>
                    <li>
                      <button type="button" className="dark:hover:text-white" onClick={() => setProfileModal(true)}>
                        <RiUserLine className="shrink-0 text-base ltr:mr-2 rtl:ml-2" />
                        My Profile
                      </button>
                    </li>
                    <li>
                      <button type="button" className="dark:hover:text-white" onClick={() => setChangePasswordModal(true)}>
                        <SlLock className="shrink-0 text-base ltr:mr-2 rtl:ml-2" />
                        Change Password
                      </button>
                    </li> */}
                    <li
                      className="cursor-pointer rounded border-t border-white-light hover:bg-[#fff] dark:border-white-light/10"
                      onClick={handleSignOut}
                    >
                      <div className="ml-4 flex cursor-pointer items-center !py-3 text-danger">
                        <FiLogOut className="shrink-0 text-base ltr:mr-2 rtl:ml-2" />
                        Sign Out
                      </div>
                    </li>
                  </ul>
                </Dropdown>
              </div>
            )}
          </div>
        </div>
      </div>

      {/* Profile Modal */}
      <GlobalModal modalTitle="Admin Profile" showHandler={profileModal} closeHandler={setProfileModal}>
        <h4 className="p-10 text-xl">I am {user?.name} 😂 🤣</h4>
        <div></div>
      </GlobalModal>

      {/* Change Password Modal */}
      <GlobalModal modalTitle="Change Password" showHandler={changePasswordModal} closeHandler={setChangePasswordModal}>
        <h4 className="p-10 text-xl">Upcoming Feature!</h4>
      </GlobalModal>
    </header>
  )
}

export default Header
