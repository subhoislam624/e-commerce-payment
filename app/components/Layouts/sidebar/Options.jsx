'use client'

import Link from 'next/link'
import { usePathname } from 'next/navigation'
import { useEffect, useState } from 'react'
import AnimateHeight from 'react-animate-height'
import { AiFillSetting, AiOutlineProduct } from 'react-icons/ai'
import { BiSolidDashboard } from 'react-icons/bi'
import { BsStarHalf } from 'react-icons/bs'
import { MdOutlineContacts } from 'react-icons/md'
import { TbCategoryPlus } from 'react-icons/tb'
import PerfectScrollbar from 'react-perfect-scrollbar'

const menuItems = [
  {
    title: 'Dashboard',
    icon: BiSolidDashboard,
    link: '/admin/dashboard',
    pathname: 'dashboard',
  },
  // {
  //   title: 'Selected',
  //   icon: BsStarHalf,
  //   pathname: 'selected',
  //   subMenu: [
  //     {
  //       title: 'Leagues',
  //       link: '/admin/selected/leagues',
  //     },
  //     {
  //       title: 'Players',
  //       link: '/admin/selected/players',
  //     },
  //     {
  //       title: 'Teams',
  //       link: '/admin/selected/teams',
  //     },
  //   ],
  // },

  {
    title: 'product',
    icon: AiOutlineProduct,
    link: '/admin/product',
    pathname: 'product',
  },
  {
    title: 'category',
    icon: TbCategoryPlus,
    link: '/admin/category',
    pathname: 'category',
  },
  // {
  //   title: 'Notifications',
  //   icon: TbBellRingingFilled,
  //   link: '/admin/notifications',
  //   pathname: 'notifications',
  // },
  // {
  //   title: 'Contacts',
  //   icon: MdOutlineContacts,
  //   link: '/admin/contact-us',
  //   pathname: 'contact-us',
  // },

  // {
  //   title: 'Administration',
  //   icon: AiFillSetting,
  //   link: '/admin/administration',
  //   pathname: 'administration',
  // },
]

export default function Options() {
  const [currentMenu, setCurrentMenu] = useState('')

  const toggleMenu = (value) => {
    setCurrentMenu((prev) => {
      return prev === value ? '' : value
    })
  }

  return (
    <PerfectScrollbar className="relative h-[calc(100vh-80px)] w-full">
      <ul className="relative space-y-0.5 p-4 py-0 font-semibold">
        {menuItems.map((item, index) => {
          if (!item?.subMenu) return <MenuItem item={item} key={index} />
          else
            return <MenuItemWithSubmenu currentMenu={currentMenu} toggleMenu={toggleMenu} setCurrentMenu={setCurrentMenu} item={item} key={index} />
        })}
      </ul>
    </PerfectScrollbar>
  )
}

function MenuItem({ item }) {
  const pathname = usePathname()

  return (
    <li className="nav-item">
      <Link href={item?.link || ''} className={`group ${pathname.includes(item?.pathname) && 'active'}`}>
        <div className="flex items-center">
          <item.icon className={`shrink-0 group-hover:!text-primary ${pathname.includes(item?.pathname) ? '!text-primary' : ''}`} />

          <span
            className={`text-black ${
              pathname.includes(item?.pathname) && 'font-bold !text-primary'
            } pl-3 dark:text-[#506690] dark:group-hover:text-white-dark`}
          >
            {item.title}
          </span>
        </div>
      </Link>
    </li>
  )
}

function MenuItemWithSubmenu({ item, currentMenu, toggleMenu, setCurrentMenu }) {
  const pathname = usePathname()

  useEffect(() => {
    if (pathname.includes(item?.pathname)) {
      setCurrentMenu(item?.pathname)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [item?.pathname, pathname])

  return (
    <li className="nav-item">
      <button
        type="button"
        className={`${currentMenu === item?.pathname ? 'active' : ''} nav-link group w-full`}
        onClick={() => toggleMenu(item?.pathname)}
      >
        <div className="flex items-center">
          <BsStarHalf className={`h-6 w-6 shrink-0 group-hover:!text-primary ${pathname.includes(item?.pathname) && '!text-primary'}`} />

          <span
            className={`pl-3 text-black dark:text-[#506690] dark:group-hover:text-white-dark ${
              pathname.includes(item?.pathname) && 'font-bold !text-primary'
            }`}
          >
            {item?.title}
          </span>
        </div>

        <div className={currentMenu === item?.pathname ? '!rotate-90' : 'rtl:rotate-180'}>
          <svg width="16" height="16" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9 5L15 12L9 19" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
          </svg>
        </div>
      </button>

      <AnimateHeight duration={300} height={currentMenu === item?.pathname ? 'auto' : 0}>
        <ul className="sub-menu text-gray-500">
          <li>
            <Link className={`group ${pathname.includes('leagues') && 'active'}`} href="/admin/selected/leagues">
              Leagues
            </Link>
          </li>
          <li>
            <Link className={`group ${pathname.includes('players') && 'active'}`} href="/admin/selected/players">
              Players
            </Link>
          </li>
          <li>
            <Link className={`group ${pathname.includes('teams') && 'active'}`} href="/admin/selected/teams">
              Teams
            </Link>
          </li>
        </ul>
      </AnimateHeight>
    </li>
  )
}
