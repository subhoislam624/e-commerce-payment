import Logo from '@/app/_components/Logo'
import Link from 'next/link'
import Options from './Options'

export default function Sidebar() {
  return (
    <nav className="sidebar fixed bottom-0 top-0 z-50 h-full min-h-screen w-[220px] shadow ">
      <div className="h-full bg-white dark:bg-black">
        <div className="flex flex-col items-center justify-between  py-3">
          <Link href="/" className="main-logo flex h-10 shrink-0 items-center">
            <Logo />
          </Link>

          {/* <button
            type="button"
            className="collapse-icon flex h-8 w-8 items-center rounded-full transition duration-300 hover:bg-gray-500/10 dark:text-white-light dark:hover:bg-dark-light/10 rtl:rotate-180"
            onClick={() => dispatch(toggleSidebar())}
          >
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="m-auto h-5 w-5">
              <path d="M13 19L7 12L13 5" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
              <path
                opacity="0.5"
                d="M16.9998 19L10.9998 12L16.9998 5"
                stroke="currentColor"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </button> */}
        </div>
        <div className="mt-3">
          <Options />
        </div>
      </div>
    </nav>
  )
}
