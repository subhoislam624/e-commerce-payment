import { backend } from '@/lib/api/getAxios'
import { Dialog, Transition } from '@headlessui/react'
import { Fragment, useState } from 'react'
import { toast } from 'react-hot-toast'
import { ImSpinner } from 'react-icons/im'

export default function AdsModalHome({ createAdsModal, setCreateAdsModal, refetch }) {
  const [isCreating, setIsCreating] = useState(false)
  const [newFolder, setNewFolder] = useState('')
  const [creatingError, setCreatingError] = useState('')

  const createFolderHandler = async () => {
    setIsCreating(true)
    if (newFolder) {
      const { data } = await backend.post('/api/v1/folder/create', {
        user_id: 'user100',
        name: newFolder,
      })

      if (data?.status === true) {
        refetch()
        setIsCreating(false)
        setNewFolder('')
        setCreateAdsModal(false)
        setCreatingError('')
        toast.success('Folder is Created!')
      } else {
        setIsCreating(false)
        setCreatingError('Folder Already Exist!')
      }
    } else {
      setIsCreating(false)
      setCreatingError('Folder Name is Required!')
    }
  }

  return (
    <div className="mt-10 flex flex-col items-center justify-center gap-5">
      <Transition appear show={createAdsModal} as={Fragment}>
        <Dialog as="div" open={createAdsModal} onClose={() => setCreateAdsModal(true)}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0" />
          </Transition.Child>
          <div className="fixed inset-0 z-[999] overflow-y-auto bg-[black]/60">
            <div className="flex min-h-screen items-center justify-center px-4">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel as="div" className="panel my-8 w-full max-w-lg overflow-hidden rounded-lg border-0 p-0 text-black dark:text-white-dark">
                  <div className="flex items-center justify-between bg-[#fbfbfb] px-5 py-3 dark:bg-[#121c2c]">
                    <h5 className="text-lg font-bold">Create New Folder</h5>
                    <button type="button" className="text-white-dark hover:text-dark" onClick={() => setCreateAdsModal(false)}>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="20"
                        height="20"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="1.5"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      >
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                      </svg>
                    </button>
                  </div>
                  <div className="p-5">
                    <h4 className="mb-1 text-justify text-sm font-medium">Folder Name</h4>
                    <div>
                      <input
                        className="w-full rounded border border-gray-200 px-4 py-2"
                        type="text"
                        name="folder_name"
                        value={newFolder}
                        onChange={(e) => setNewFolder(e.target.value)}
                      />
                      {creatingError && <div className="mt-1 text-xs text-danger">{creatingError}</div>}
                    </div>

                    <div className="mt-8 flex items-center justify-end">
                      <button type="button" className="btn btn-primary btn-sm ltr:ml-4 rtl:mr-4" disabled={isCreating} onClick={createFolderHandler}>
                        Create
                        {isCreating && <ImSpinner className="animate-spin text-base" />}
                      </button>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </div>
  )
}
