import { Dialog, Transition } from '@headlessui/react'
import { Fragment, useState } from 'react'

import { backend } from '@/lib/api/getAxios'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import toast from 'react-hot-toast'
import * as Yup from 'yup'

const validationSchema = Yup.object().shape({
  name: Yup.string().required('Name is required').max(255, 'Name must be at most 255 characters'),
})

export default function UpdateAdsModal({ folder, updateAdsModal, setUpdateAdsModal, refetch }) {
  const [isUpdating, setIsUpdating] = useState(false)

  const handleUpdateFolder = async (values) => {
    try {
      setIsUpdating(true)

      const newData = { ...values }
      const { data } = await backend.put(`/api/v1/folder/${values._id}`, { newData })
      if (data.status) {
        refetch()
        setIsUpdating(false)
        setUpdateAdsModal(false)
        toast.success('QR Folder updated successfully')
      }
    } catch (error) {
      setIsUpdating(false)
    }
  }

  return (
    <Transition appear show={updateAdsModal} as={Fragment}>
      <Dialog as="div" open={updateAdsModal} onClose={() => setUpdateAdsModal(true)}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0" />
        </Transition.Child>
        <div className="fixed inset-0 z-[999] overflow-y-auto bg-[black]/60">
          <div className="flex min-h-screen items-center justify-center px-4">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel as="div" className="panel my-8 w-full max-w-lg overflow-hidden rounded-lg border-0 p-0 text-black dark:text-white-dark">
                <div className="flex items-center justify-between bg-[#fbfbfb] px-5 py-3 dark:bg-[#121c2c]">
                  <h5 className="text-lg font-bold">
                    {isUpdating ? `Updating ${folder.name} Folder . . .` : `You are updating ${folder.name} Folder`}{' '}
                  </h5>
                  <button type="button" className="text-gray-700 hover:text-red-500" onClick={() => setUpdateAdsModal(false)}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth="1.5"
                      stroke="currentColor"
                      className="h-6 w-6"
                    >
                      <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                  </button>
                </div>
                <div className="p-5">
                  <Formik initialValues={folder} validationSchema={validationSchema} onSubmit={handleUpdateFolder}>
                    {() => (
                      <Form>
                        <div className="mb-2">
                          <label htmlFor="name" className="mt-2 text-[14px] font-semibold text-gray-800">
                            Folder Name:
                          </label>
                          <Field type="text" id="name" name="name" className="w-full rounded-lg border border-gray-300 px-4 py-2" />
                          <ErrorMessage name="name" component="div" className="text-red-600" />
                        </div>

                        <div className="mt-3 flex items-center justify-end gap-3">
                          <button onClick={() => setUpdateAdsModal(false)} type="button" className="btn btn-info btn-sm">
                            Cancel
                          </button>
                          <button type="submit" className="btn btn-success btn-sm">
                            Update
                          </button>
                        </div>
                      </Form>
                    )}
                  </Formik>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  )
}
