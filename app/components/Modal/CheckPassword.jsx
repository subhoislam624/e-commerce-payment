'use client'

import { Dialog, Transition } from '@headlessui/react'
import { useRouter } from 'next/navigation'
import { Fragment, useEffect, useState } from 'react'
import { BsEyeFill, BsEyeSlashFill } from 'react-icons/bs'
import { ImSpinner } from 'react-icons/im'

export default function CheckPassword({ password, qr_id }) {
  const [givenPassword, setGivenPassword] = useState('')
  const [passwordModal, setPasswordModal] = useState(false)
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [showPassword, setShowPassword] = useState(false)
  const [validPassword, setValidPassword] = useState(false)

  const { push } = useRouter()

  useEffect(() => {
    setPasswordModal(true)
  }, [])

  const checkPassword = () => {
    setIsSubmitting(true)
    if (password == givenPassword) {
      push(`/p/${qr_id}`)
    } else {
      setIsSubmitting(false)
      setValidPassword(true)
    }
  }

  return (
    <div className="mt-10 flex flex-col items-center justify-center gap-5">
      <Transition appear show={passwordModal} as={Fragment}>
        <Dialog as="div" open={passwordModal} onClose={() => setPasswordModal(true)}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0" />
          </Transition.Child>
          <div className="fixed inset-0 z-[999] overflow-y-auto bg-[black]/60">
            <div className="flex min-h-screen items-center justify-center px-4">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel as="div" className="panel my-8 w-full max-w-lg overflow-hidden rounded-lg border-0 p-0 text-black dark:text-white-dark">
                  <div className="flex items-center justify-between bg-[#fbfbfb] px-5 py-3 dark:bg-[#121c2c]">
                    <h5 className="text-lg font-bold">Password Required</h5>
                  </div>
                  <div className="p-5">
                    <p className="mb-3 text-justify">If you want to access this QR you must enter its corresponding password.</p>
                    <div className="relative">
                      <input
                        className="w-full rounded border border-gray-200 px-4 py-2"
                        type={showPassword ? 'text' : 'password'}
                        name="password"
                        value={givenPassword}
                        onChange={(e) => setGivenPassword(e.target.value)}
                      />
                      {validPassword && <div className="mt-1 text-xs text-danger">Incorrect Password!</div>}

                      <BsEyeFill
                        className={`absolute right-3 top-3 ${showPassword ? '' : 'hidden'} cursor-pointer text-xl text-gray-600`}
                        onClick={() => setShowPassword(false)}
                      />
                      <BsEyeSlashFill
                        className={`absolute right-3 top-3 ${showPassword ? 'hidden' : ''} cursor-pointer text-xl text-gray-600`}
                        onClick={() => setShowPassword(true)}
                      />
                    </div>

                    <div className="mt-8 flex items-center justify-end">
                      <button type="button" className="btn btn-primary btn-sm ltr:ml-4 rtl:mr-4" onClick={checkPassword} disabled={isSubmitting}>
                        Enter
                        {isSubmitting && <ImSpinner className="animate-spin text-sm" />}
                      </button>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </div>
  )
}
