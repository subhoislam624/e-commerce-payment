import { backend } from '@/lib/api/getAxios'
import { Dialog, Transition } from '@headlessui/react'
import { Fragment, useState } from 'react'
import toast from 'react-hot-toast'

export default function DeleteAdsModal({ folder, deleteAdsModal, setDeleteAdsModal, refetch }) {
  const [isDeleting, setIsDeleting] = useState(false)

  const handleQrDelete = async (id) => {
    try {
      setIsDeleting(true)
      const { data } = await backend.delete(`/api/v1/folder/${id}`)
      if (data.status) {
        refetch()
        setIsDeleting(false)
        setDeleteAdsModal(false)
        toast.success('QR Folder deleted successfully')
      }
    } catch (error) {
      setIsDeleting(false)
    }
  }

  return (
    <Transition appear show={deleteAdsModal} as={Fragment}>
      <Dialog as="div" open={deleteAdsModal} onClose={() => setDeleteAdsModal(true)}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0" />
        </Transition.Child>
        <div className="fixed inset-0 z-[999] overflow-y-auto bg-[black]/60">
          <div className="flex min-h-screen items-center justify-center px-4">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel as="div" className="panel my-8 w-full max-w-lg overflow-hidden rounded-lg border-0 p-0 text-black dark:text-white-dark">
                <div className="flex items-center justify-between bg-[#fbfbfb] px-5 py-3 dark:bg-[#121c2c]">
                  <h5 className="text-lg font-bold">
                    {isDeleting ? `Deleting ${folder.name} Folder . . .` : `Are you sure you want to delete ${folder.name} Folder?`}{' '}
                  </h5>
                  <button type="button" className="text-gray-700 hover:text-red-500" onClick={() => setDeleteAdsModal(false)}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth="1.5"
                      stroke="currentColor"
                      className="h-6 w-6"
                    >
                      <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                  </button>
                </div>
                {isDeleting ? (
                  <div className="p-3">
                    <div className="flex  items-center justify-center">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="#D35400"
                        className="h-24 w-24 animate-ping rounded-full bg-red-300 p-2"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                        />
                      </svg>
                    </div>
                    <div className="mt-3 flex items-center justify-end gap-3">
                      <button disabled type="button" className="btn btn-info btn-sm">
                        Cancel
                      </button>
                      <button disabled type="button" className="btn btn-error btn-sm">
                        Confirm
                      </button>
                    </div>
                  </div>
                ) : (
                  <div className="p-3">
                    <div className="flex  items-center justify-center">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="#CB4335"
                        className="h-24 w-24 rounded-full bg-gray-200 p-2"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                        />
                      </svg>
                    </div>
                    <div className="mt-3 flex items-center justify-end gap-3">
                      <button onClick={() => setDeleteAdsModal(false)} type="button" className="btn btn-info btn-sm">
                        Cancel
                      </button>
                      <button onClick={() => handleQrDelete(folder?._id)} type="button" className="btn btn-error btn-sm">
                        Confirm
                      </button>
                    </div>
                  </div>
                )}
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  )
}
