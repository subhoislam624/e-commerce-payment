'use client'

import store from '@/store/index'
import { SessionProvider } from 'next-auth/react'
import NextTopLoader from 'nextjs-toploader'
import { QueryClient, QueryClientProvider } from 'react-query'
import { Provider } from 'react-redux'

function Providers({ children, session }) {
  const queryClient = new QueryClient()

  return (
    <Provider store={store}>
      <NextTopLoader color="#105A6D" showSpinner={false} />
      <SessionProvider session={session}>
        <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
      </SessionProvider>
    </Provider>
  )
}

export default Providers
