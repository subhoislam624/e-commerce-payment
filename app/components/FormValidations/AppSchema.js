import * as Yup from 'yup';

const appSchema = () =>
  Yup.object().shape({
    app_name: Yup.string().required('App Name Required!'),
    app_unique_id: Yup.string().required('App Unique Id Required!'),
    sportapi_base_url: Yup.string().required('Sport API Base URL Required!'),
    sportapi_api_key: Yup.string().required('Sport API Key Required!'),
    android_onesignal_app_id: Yup.string().when('android_notification_type', {
      is: 'onesignal',
      then: ()=>Yup.string().required('Onesignal App ID Required!')
    }),
    android_onesignal_api_key: Yup.string().when('android_notification_type', {
      is: 'onesignal',
      then: ()=>Yup.string().required('Onesignal API Key Required!')
    }),
    android_firebase_server_key: Yup.string().when('android_notification_type', {
      is: 'fcm',
      then: ()=>Yup.string().required('Firebase Server Key Required!')
    }),
    android_firebase_topics: Yup.string().when('android_notification_type', {
      is: 'fcm',
      then: ()=>Yup.string().required('Firebase Topics Required!')
    }),
    ios_onesignal_app_id: Yup.string().when('ios_notification_type', {
      is: 'onesignal',
      then: ()=>Yup.string().required('Onesignal App ID Required!')
    }),
    ios_onesignal_api_key: Yup.string().when('ios_notification_type', {
      is: 'onesignal',
      then: ()=>Yup.string().required('Onesignal API Key Required!')
    }),
    ios_firebase_server_key: Yup.string().when('ios_notification_type', {
      is: 'fcm',
      then: ()=>Yup.string().required('Firebase Server Key Required!')
    }),
    ios_firebase_topics: Yup.string().when('ios_notification_type', {
      is: 'fcm',
      then: ()=>Yup.string().required('Firebase Topics Required!')
    })
  });

export default appSchema;
