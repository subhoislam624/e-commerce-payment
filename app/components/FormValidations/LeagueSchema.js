import * as Yup from "yup";

const leagueSchema = () =>
  Yup.object().shape({
    name: Yup.string().required("Name is required!"),
    image: Yup.string(),
    status: Yup.string().required("Status is required!"),
  });

export default leagueSchema;
