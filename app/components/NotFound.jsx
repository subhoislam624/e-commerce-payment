'use client';

import { useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';
import { BsQrCodeScan } from 'react-icons/bs';
import { IoQrCodeOutline, IoQrCodeSharp } from 'react-icons/io5';
import { MdOutlineQrCode, MdQrCode } from 'react-icons/md';

const icons = [
    <BsQrCodeScan key='bs' className='animate-ping text-7xl text-blue-500' />,
    <MdOutlineQrCode key='md1' className=' animate-ping text-7xl text-green-500' />,
    <IoQrCodeOutline key='io1' className='animate-ping text-7xl text-purple-500' />,
    <MdOutlineQrCode key='md1' className=' animate-ping text-7xl text-green-500' />,
    <IoQrCodeSharp key='io2' className='animate-ping text-7xl text-red-500' />,
    <MdQrCode key='md3' className=' animate-ping text-7xl text-sky-500' />,
    <MdQrCode key='md5' className='animate-ping text-7xl text-red-500' />,
    <MdQrCode key='md4' className='animate-ping text-7xl text-violet-500' />
];

export default function NotFound() {
    const { push } = useRouter();
    const [currentIconIndex, setCurrentIconIndex] = useState(0);

    useEffect(() => {
        const interval = setInterval(() => {
            setCurrentIconIndex(prevIndex => (prevIndex + 1) % icons.length);
        }, 500);

        return () => clearInterval(interval);
    }, []);

    const goToHome = () => {
        push('/');
    };

    return (
        <div className='flex h-screen w-screen items-center justify-center bg-[#022A5A]'>
            <div className='flex flex-col items-center justify-center'>
                <div>{icons[currentIconIndex]}</div>
                <div className='p-10 text-center text-gray-200'>
                    <h2 className='text-3xl font-semibold text-white'>404 Error</h2>
                    <p className='text-gray-300'>Oops! The page you&apos;re looking for does not exist.</p>
                </div>
                <button
                    onClick={goToHome}
                    className='mt-4 rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 focus:outline-none focus:ring focus:ring-blue-300'
                >
                    Powered By <span className='font-bold text-gray-300'> RootQR</span>
                </button>
            </div>
        </div>
    );
}
