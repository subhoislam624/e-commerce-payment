import { useQuery } from 'react-query'

export default function TopLeaguesSection() {
  const {
    data: leagues,
    error: leaguesError,
    isLoading: leaguesLoading,
    refetch: leaguesRefetch,
  } = useQuery(
    'leagues',
    async () => {
      const { data } = await backend.get(`api/app/leagues`)
      return data?.data
    },
    {
      cacheTime: 0,
    },
  )

  return (
    <div className="mx-auto mt-10 max-w-screen-xl overflow-hidden px-4">
      {/* <Marquee speed={150} pauseOnHover={true} loop={0}> */}
      <div className="scrollbar-hidden flex w-full justify-between gap-5 overflow-y-auto">
        {leaguesLoading
          ? [...Array(5)].map((_, index) => (
              <div key={index} className="h-16 w-16">
                <SkeletonLoader />
              </div>
            ))
          : leagues?.map((league) => (
              <div key={league.id} className="min-w-20">
                <img src={league?.logo} alt={league?.name} className="h-16 w-16 rounded-full" />
              </div>
            ))}
      </div>
      {/* </Marquee> */}
    </div>
  )
}

const SkeletonLoader = () => {
  return <div className="h-full w-full animate-pulse rounded-full bg-gray-300 dark:bg-gray-600"></div>
}
