export default function ReviewSection() {
  return (
    <section id='review' className='mt-10'>
      <div className='text-center'>
        <h4 className='text-3xl font-semibold text-gray-900 dark:text-gray-50 md:text-5xl'>Customers Say</h4>
        <p className='mt-4 p-2 font-light text-gray-900 dark:text-gray-300'>
          Browse our customer reviews to see for yourself what sets us apart and why we{"'"}re the go-to choice for
          interior design services.
        </p>
      </div>
      <div className='bg-gray-900 dark:bg-gray-700'>
        <div className='mt-4 grid grid-cols-1 px-2 md:grid-cols-3 md:px-10'>
          <div className='m-6 rounded p-2'>
            <div className='flex items-center gap-2'>
              <img className='w-10' src='https://cdn-icons-png.flaticon.com/512/149/149071.png' alt='' />
              <h5 className='text-sm font-semibold text-gray-50'>Jane Doe</h5>
            </div>
            <p className='mt-2 p-1 font-light text-gray-50'>
              I recently worked with the team at [website name] to redesign my living room, and I couldn{"'"}t be
              happier with the results. The designer listened to my vision and was able to create a space that exceeded
              my expectations. They were professional, efficient, and always available to answer any questions I had. I
              highly recommend them to anyone looking to update their space.
            </p>
          </div>
          <div className='m-6 rounded bg-gray-50 p-2 text-gray-800'>
            <div className='flex items-center gap-2'>
              <img className='w-10' src='https://cdn-icons-png.flaticon.com/512/149/149071.png' alt='' />
              <h5 className=' text-xl font-semibold text-gray-800'>John Smith</h5>
            </div>
            <p className='mt-2 p-1 font-light text-gray-800'>
              I{"'"}ve been a customer of [website name] for several years now, and I{"'"}ve always been impressed with
              the quality of their work. They have helped me redesign multiple rooms in my home and each one is better
              than the last. The team is professional, knowledgeable, and always willing to go the extra mile to make
              sure I{"'"}m happy with the final result. I can{"'"}t recommend them enough.
            </p>
          </div>
          <div className='m-6 rounded p-2'>
            <div className='flex items-center gap-2'>
              <img className='w-10' src='https://cdn-icons-png.flaticon.com/512/149/149071.png' alt='' />
              <h5 className=' text-xl font-semibold text-gray-50'>Sarah Johnson</h5>
            </div>
            <p className='mt-2 p-1 font-light text-gray-50'>
              I had a great experience working with [website name] on my kitchen remodel. The designer helped me make
              the most of my small space and created a functional and beautiful layout that I love. They were always
              willing to make changes and revisions until I was completely satisfied with the final design. I highly
              recommend their services.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}
