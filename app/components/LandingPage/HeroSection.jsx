'use client'

export default function HeroSection({ settings }) {
  return (
    <section id="home" className="mx-auto mt-10 max-w-screen-xl px-1 transition duration-300 ease-in-out md:px-10">
      <div className="grid grid-cols-1 md:grid-cols-2">
        <div className="order-2 flex flex-col items-start justify-center gap-4 p-3 md:order-1 md:p-10">
          <h4 className="text-4xl font-medium leading-tight md:text-6xl">Immerse Yourself in the Exciting Universe of Football</h4>

          <p>
            Welcome to our vibrant football universe! Immerse yourself in the exciting world of football with our innovative app. Whether you are a
            seasoned fan or new to the sport, our app is your ultimate companion. It is designed to deliver immersive experiences, insider insights,
            and everything else you need to embark on an unforgettable football adventure.
          </p>

          <div className="mb-4 flex items-center justify-center space-x-0 md:mb-8 md:space-x-2 lg:justify-start">
            <a
              href={settings?.android_download_link}
              className="mx-2 flex w-36 cursor-pointer items-center rounded-lg border px-1 py-2 lg:w-44 lg:px-4"
            >
              <img src="./playStore.svg" className="w-7 md:w-8" alt="Play Store Icon" />
              <div className="ml-3 text-left">
                <p className="text-xs text-gray-400">Download on </p>
                <p className="text-sm md:text-base"> Play Store </p>
              </div>
            </a>
            <a href={settings?.ios_download_link} className="mx-2 flex w-36 cursor-pointer items-center rounded-lg border px-1 py-2 lg:w-44 lg:px-4">
              <img src="./appStore.svg" className="w-7 md:w-8" alt="Play Store Icon" />
              <div className="ml-3 text-left">
                <p className="text-xs text-gray-400">Download on </p>
                <p className="text-sm md:text-base"> Apple Store </p>
              </div>
            </a>
          </div>
        </div>
        <div className="flex items-center justify-center md:order-2">
          <img className="w-72 md:w-full md:p-10" src="./app-screen-01.png" alt="" />
        </div>
      </div>
    </section>
  )
}
