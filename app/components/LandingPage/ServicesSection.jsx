import { BsMicrosoftTeams } from "react-icons/bs";
import { CiMoneyCheck1 } from "react-icons/ci";
import { RiLiveFill } from "react-icons/ri";

export default function ServicesSection() {
  return (
    <section
      id='services'
      className='mx-auto mt-10 max-w-screen-xl px-2 py-20 transition duration-300 ease-in-out md:px-10'
    >
      <div className='text-center'>
        <h4 className='text-3xl font-semibold text-gray-900 dark:text-gray-50 md:text-5xl'>Discover What We Offer</h4>
        <p className='mt-4 text-base font-medium text-gray-700 dark:text-gray-100 md:text-lg'>
          Our football app provides a comprehensive range of features to enhance your football experience, tailored to
          match your passion and preferences.
        </p>
      </div>
      <div className='mt-4 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3'>
        <div className='mx-auto mb-4 flex min-h-[350px] w-full flex-col items-start justify-between rounded border-2 border-gray-400 p-4 md:w-full md:max-w-[300px]'>
          <svg width='120' height='120' viewBox='0 0 256 256' xmlns='http://www.w3.org/2000/svg'>
            <path
              d='M233 30.87c-3.17-.63-206.32-.52-210.51 0-5.36.67-7.69 5.48-8.18 9.2-.4 3.09.4 82.93.59 120.93h225.86c.26-40 .62-119-.29-122-1.33-4.22-4.08-7.45-7.47-8.13'
              fill='#191919'
            />
            <path
              d='M240.76 169H14.88a8 8 0 0 1-8-8c-.06-12.35-.19-29.13-.31-46.25-.45-59.42-.49-73.57-.21-75.75 1.14-8.78 7.07-15.1 15.12-16.11 3.48-.44 82.5-.49 106.63-.5 103.38 0 105.44.39 106.44.59 6.2 1.24 11.27 6.33 13.56 13.63.57 1.82 1.44 4.58.66 124.4a8 8 0 0 1-8.01 7.99M22.84 153h210c.38-62.46.32-105.65-.17-112.12a5.7 5.7 0 0 0-1.26-2.09c-15.14-.48-197.4-.46-208 0-.66.19-1 1.57-1.12 2.13-.2 4.31.09 42.74.32 73.71.06 13.73.16 27.2.23 38.37'
              fill='#191919'
            />
            <path
              d='M175.3 219.81c-.77-.59-22.51-17.14-23.17-17.84s-8.51-13.63-8.7-22.87c-.29-14.38-38-16.05-38.54.18-.28 8.36-7.12 20.73-8 21.36S75.23 219 74.7 219.81c-1 1-.57 10.57-.11 11s100 .94 100.83.26.67-11.07-.12-11.26'
              fill='#191919'
            />
            <path
              d='M145.57 239.43h-20.72c-12.6 0-25.18-.13-34.51-.23-18.94-.2-18.94-.2-21.41-2.7s-2.74-5.11-2.82-10.87c-.08-5.46.37-8.91 2.93-11.47l.06.06c4.19-4.22 17.85-15.69 21.9-19.08 1.67-2.72 5.7-11.27 5.86-16.12.39-11.65 11.24-19.45 26.94-19.31 16 .1 27.37 8 27.59 19.24.11 5.37 4.68 14.41 6.67 17.59 1.62 1.28 6.54 5.1 20.79 16l1.07.82a8.68 8.68 0 0 1 3.45 5.46 41.3 41.3 0 0 1 .54 7.76c0 4.45-.33 8.06-3.33 10.64-2.51 2.15-2.51 2.15-21.19 2.24-3.97-.03-8.65-.03-13.82-.03m27.28-12.01.25.08zM83 223.14c18.63.29 65.22.43 83.81.25-19.45-14.84-19.83-15.25-20.45-15.91-1.44-1.54-10.62-16.33-10.86-28.19-.71-1.68-8.3-4.65-16.37-3.17-2.94.54-6.12 1.85-6.18 3.45-.29 8.91-7 24.26-11.13 27.46-1.62 1.21-13.57 11.41-18.88 16.07zm9.26-28.93a8 8 0 0 0-.69.57z'
              fill='#191919'
            />
            <path
              d='M15 178.71c0 3.76 1.83 10.14 8.18 12.26 5.4 1.81 206.1-.14 209.83-.91a9.85 9.85 0 0 0 7.49-9c.11-1.21.87-21 .29-22.14-.3-.57-59.58.25-117.24.29-54.87 0-108.26-.7-108.64-.28-.82.92.09 19.07.09 19.78'
              fill='#191919'
            />
            <path
              d='M69.15 199.73c-45 0-47.07-.68-48.54-1.17C11.23 195.42 7 186.39 7 178.8v-1.25c-.79-19.61-.51-21.12 2-23.92s5.1-2.72 8.5-2.76h7.31c6.18 0 15 .07 25.52.13 19.62.12 46.49.3 73.26.27 24.79 0 50.11-.18 70.46-.32 13.57-.09 25.29-.16 33.57-.18h9.79c4.43.06 8.25.11 10.52 4.46 1 2 1.32 3.08 1 14.94-.12 5-.35 10.65-.43 11.61a17.92 17.92 0 0 1-13.85 16.1c-4.21.87-101.7 1.56-105.85 1.58-25.53.2-44.9.27-59.65.27m-43.92-16.54c5.3.44 35.2.77 104.05.29 51.68-.36 96.08-1 102.24-1.32a2.08 2.08 0 0 0 1-1.77c.09-1.24.34-8.18.44-13.63-8.41 0-23.34.07-38.87.17-20.37.13-45.71.3-70.55.32-26.82 0-53.72-.15-73.37-.27-11.07-.07-21.17-.13-27.57-.14 0 2.37.15 5.62.32 10.07 0 1 .06 1.67.06 1.8s.02 3.29 2.25 4.48'
              fill='#191919'
            />
            <path
              d='M233 30.87c-3.17-.63-206.32-.52-210.51 0-5.36.67-7.69 5-8.18 8.7-.4 3.09.4 82.43.59 120.43h225.86c.26-40 .62-118.55-.29-121.45-1.33-4.27-4.08-7-7.47-7.68'
              fill='#e83a2a'
            />
            <path
              d='M175.3 219.81c-.77-.59-22.51-17.14-23.17-17.84s-8.51-13.63-8.7-22.87c-.29-14.38-38-16.05-38.54.18-.28 8.36-7.12 20.73-8 21.36S75.23 219 74.7 219.81c-1 1-.57 10.57-.11 11s100 .94 100.83.26.67-11.07-.12-11.26'
              fill='#fff'
            />
            <path
              d='M15 178.71c0 3.76 1.83 10.14 8.18 12.26 5.4 1.81 206.1-.14 209.83-.91a9.85 9.85 0 0 0 7.49-9c.11-1.21.87-21 .29-22.14-.3-.57-59.58.25-117.24.29-54.87 0-108.26-.7-108.64-.28-.82.92.09 19.07.09 19.78M90.58 76.43c-5.11 5.75-5.44 44.83 1.15 50.58 3.26 2.84 49.93 2.63 52.23 0s2.47-15.27 5.11-12.64 10.88 9.69 13.35 11.49 5.4 2.54 6.43-3.28c1-5.58.66-42.53.16-46.47-.45-3.59-4.78-3.28-7.58-.66s-11 9.69-13 11.49-1.81-9-4.61-11.33-48.96-4-53.24.82'
              fill='#fff'
            />
          </svg>
          <h4 className='text-3xl font-semibold md:text-4xl'>Live Match Analysis</h4>
          <p className='text-sm'>
            Dive into our live match analysis feature, where you gain exclusive insights into ongoing matches. We
            provide in-depth statistics, tactical breakdowns, and expert commentary to keep you informed and engaged
            throughout every game.
          </p>
        </div>
        <div className='mx-auto mb-4 flex min-h-[350px] w-full flex-col items-start justify-between rounded border-2 border-gray-400 bg-gray-800 p-4 text-gray-50 md:max-w-[300px]'>
          <svg width='120' height='120' viewBox='-0.5 0 80 80' xmlns='http://www.w3.org/2000/svg'>
            <g data-name='Group 8'>
              <g data-name='Group 7'>
                <path
                  data-name='Path 10'
                  d='M480.03 97.331v49.811a3 3 0 0 1-3 3h-59.35a16.67 16.67 0 0 1-16.65-16.65v-43.74c0-9.49 7.741-16.65 18-16.65a3 3 0 0 1 3 3v18.229h23.991V73.142a3 3 0 0 1 3-3h15.309a3 3 0 0 1 3 3v21.189h9.7a3 3 0 0 1 3 3m-6 46.811v-43.811h-6.7v26.6a2.98 2.98 0 0 1-.6 1.8l-7.671 10.231a3 3 0 0 1-4.8 0l-7.64-10.2a3 3 0 0 1-.6-1.8v-26.631H422.03v19.5a3 3 0 0 1-3 3c-6.95 0-12 4.48-12 10.661a10.666 10.666 0 0 0 10.65 10.65Zm-12.7-23.571V89.462h-9.309v31.13Zm0-37.1v-7.33h-9.309v7.33Zm-4.67 48.69 4.2-5.591-8.379.021Zm-40.63-15.121V79.392c-5.36 1.1-9 5.12-9 10.36v31.12a18.43 18.43 0 0 1 9-3.831Z'
                  fill='#104386'
                  transform='translate(-401.03 -70.142)'
                />
                <path
                  data-name='Path 11'
                  d='M474.03 100.331v43.811h-56.35a10.666 10.666 0 0 1-10.65-10.65c0-6.181 5.05-10.661 12-10.661a3 3 0 0 0 3-3v-19.5h23.991v26.631a3 3 0 0 0 .6 1.8l7.64 10.2a3 3 0 0 0 4.8 0l7.671-10.231a2.98 2.98 0 0 0 .6-1.8v-26.6Z'
                  fill='#f60'
                  transform='translate(-401.03 -70.142)'
                />
                <path
                  data-name='Path 12'
                  d='M461.33 89.462v31.109l-9.31.021v-31.13Z'
                  fill='#fff'
                  transform='translate(-401.03 -70.142)'
                />
                <path
                  data-name='Rectangle 6'
                  fill='#69f'
                  d='M452.021 76.142h9.31v7.33h-9.31z'
                  transform='translate(-401.03 -70.142)'
                />
                <path
                  data-name='Path 13'
                  d='M416.03 79.392v37.649a18.43 18.43 0 0 0-9 3.831v-31.12c0-5.24 3.64-9.26 9-10.36'
                  fill='#69f'
                  transform='translate(-401.03 -70.142)'
                />
              </g>
            </g>
          </svg>

          <h4 className='text-3xl font-semibold md:text-4xl'>Game Strategy</h4>
          <p className='text-sm'>
            Gain access to our game strategy feature, where we provide detailed plans, formations, and tactics for
            upcoming matches. Whether you{"'"}re a coach, player, or fan, our comprehensive strategies will help you
            understand the game better and stay ahead of the competition.
          </p>
        </div>
        <div className='mx-auto mb-4 flex min-h-[350px] w-full flex-col items-start justify-between rounded border-2 border-gray-400 p-4 md:max-w-[300px]'>
          <svg width='130' height='130' viewBox='0 0 15 15' xmlns='http://www.w3.org/2000/svg'>
            <path d='M11 1.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0m0 9.5a1 1 0 1 0 0 2 1 1 0 0 0 0-2m1.84-4.91-1.91-1.91a.48.48 0 0 0-.37-.18H3.5a.5.5 0 0 0 0 1h2.7L3 11.3a.5.5 0 0 0 0 .2.511.511 0 0 0 1 .21L5 10h2l-1.93 4.24a.5.5 0 0 0-.07.26.51.51 0 0 0 1 .2l4.7-9.38 1.44 1.48a.5.5 0 0 0 .7-.71' />
          </svg>
          <h4 className='text-3xl font-semibold md:text-4xl'>Player Positioning</h4>
          <p className='text-sm'>
            Explore our player positioning feature, where we analyze and optimize player placement on the field. By
            considering factors like game tactics, opponent strategies, and player strengths, we ensure optimal
            positioning for a competitive edge in every match.
          </p>
        </div>
        <div className='mx-auto mb-4 flex min-h-[350px] w-full flex-col items-start justify-between rounded border-2 border-gray-400 bg-gray-800 p-4 text-gray-50 md:max-w-[300px]'>
          <CiMoneyCheck1 className='text-8xl' />

          <h4 className='text-3xl font-semibold md:text-4xl'>Kit Selection</h4>
          <p className='text-sm'>
            Explore our kit selection feature, where we assist you in choosing the perfect attire for your football
            team. From jerseys to shorts, we ensure that your team looks and feels their best on the field, with options
            that match your team{"'"}s style and budget.
          </p>
        </div>
        <div className='mx-auto mb-4 flex min-h-[350px] w-full flex-col items-start justify-between rounded border-2 border-gray-400 p-4 md:max-w-[300px]'>
          <BsMicrosoftTeams className='text-8xl' />
          <h4 className='text-3xl font-semibold md:text-4xl'>Team Management</h4>
          <p className='text-sm'>
            Explore our team management feature, where we assist you in overseeing every aspect of your football team.
            From scheduling matches to coordinating training sessions and managing player rosters, we ensure that your
            team operates smoothly and effectively both on and off the field.
          </p>
        </div>

        <div className='mx-auto mb-4 flex min-h-[350px] w-full flex-col items-start justify-between rounded border-2 border-gray-400 bg-gray-800 p-4 text-gray-50 md:max-w-[300px]'>
          <RiLiveFill className='text-8xl' />

          <h4 className='text-3xl font-semibold md:text-4xl'>Virtual Coaching</h4>
          <p className='text-sm'>
            Experience our virtual coaching service, designed to provide comprehensive football training and guidance
            from the comfort of your own home. Through video conferencing and interactive sessions, our expert coaches
            will help you improve your skills, tactics, and game understanding, ensuring that you stay at the top of
            your game.
          </p>
        </div>
      </div>
    </section>
  );
}
