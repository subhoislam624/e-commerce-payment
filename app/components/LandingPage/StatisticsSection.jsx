export default function StatisticsSection() {
  return (
    <section className='mt-10'>
      <div className='text-center'>
        <h4 className='text-3xl font-semibold text-gray-900 dark:text-gray-50 md:text-5xl'>Business Statistics</h4>
        <p className='mt-4 font-light text-gray-900 dark:text-gray-300'>
          See the results of our hard work and dedication to delivering exceptional service
        </p>
      </div>
      <div className='container mx-auto px-5 md:px-20'>
        <div className='flex w-full flex-col'>
          <div className='my-2 grid w-full grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4'>
            <div className='metric-card w-full rounded border border-gray-200 bg-gray-900 p-4 dark:border-gray-800 dark:bg-white md:max-w-72'>
              <a
                aria-label='YouTube Subscribers'
                target='_blank'
                rel='noopener noreferrer'
                href='https://stackdiary.com/'
              >
                <div className='flex items-center justify-center text-xl font-semibold text-gray-100 dark:text-gray-900'>
                  On going Projects
                  <svg
                    className='ml-1 h-6 w-6'
                    xmlns='http://www.w3.org/2000/svg'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'
                  >
                    <path
                      stroke-linecap='round'
                      stroke-linejoin='round'
                      stroke-width='2'
                      d='M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14'
                    ></path>
                  </svg>
                </div>
              </a>
              <p className='spacing-sm mt-2 text-center text-3xl font-bold text-white dark:text-black'>56</p>
            </div>
            <div className='metric-card w-full rounded border border-gray-200 bg-white p-4 dark:border-gray-800 dark:bg-gray-900 md:max-w-72'>
              <a aria-label='YouTube Views' target='_blank' rel='noopener noreferrer' href='https://stackdiary.com/'>
                <div className='flex items-center justify-center text-xl font-semibold text-gray-900 dark:text-gray-100'>
                  Completed Projects
                  <svg
                    className='ml-1 h-6 w-6'
                    xmlns='http://www.w3.org/2000/svg'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'
                  >
                    <path
                      stroke-linecap='round'
                      stroke-linejoin='round'
                      stroke-width='2'
                      d='M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14'
                    ></path>
                  </svg>
                </div>
              </a>
              <p className='spacing-sm mt-2 text-center text-3xl font-bold text-black dark:text-white'>15</p>
            </div>
            <div className='metric-card w-full rounded border border-gray-200 bg-gray-900 p-4 dark:border-gray-800 dark:bg-white md:max-w-72'>
              <a aria-label='Unsplash Views' target='_blank' rel='noopener noreferrer' href='https://stackdiary.com/'>
                <div className='flex items-center justify-center text-xl font-semibold text-gray-100 dark:text-gray-900'>
                  Email Subscribers
                  <svg
                    className='ml-1 h-6 w-6'
                    xmlns='http://www.w3.org/2000/svg'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'
                  >
                    <path
                      stroke-linecap='round'
                      stroke-linejoin='round'
                      stroke-width='2'
                      d='M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14'
                    ></path>
                  </svg>
                </div>
              </a>
              <p className='spacing-sm mt-2 text-center text-3xl font-bold text-white dark:text-black'>3,641</p>
            </div>

            <div className='metric-card w-full rounded border border-gray-200 bg-white p-4 dark:border-gray-800 dark:bg-gray-900 md:max-w-72'>
              <a
                aria-label='Unsplash Downloads'
                target='_blank'
                rel='noopener noreferrer'
                href='https://stackdiary.com/'
              >
                <div className='flex items-center justify-center text-xl font-semibold text-gray-900 dark:text-gray-100'>
                  Twitter Followers
                  <svg
                    className='ml-1 h-6 w-6'
                    xmlns='http://www.w3.org/2000/svg'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'
                  >
                    <path
                      stroke-linecap='round'
                      stroke-linejoin='round'
                      stroke-width='2'
                      d='M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14'
                    ></path>
                  </svg>
                </div>
              </a>
              <p className='spacing-sm mt-2 text-center text-3xl font-bold text-black dark:text-white'>5,412</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
