
export default function LandingPageHeader() {
  // const { settings, settingsLoading } = useGetSettings()

  return (
    <header className="sticky top-0 z-30 bg-white text-gray-800 shadow dark:bg-gray-500">
      {/* <div className="navbar px-1 md:px-10">
        <div className="navbar-start">
          <div className="dropdown">
            <label tabindex="0" className="btn btn-ghost lg:hidden">
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="#273746">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h8m-8 6h16" />
              </svg>
            </label>
            <ul
              tabindex="0"
              className="menu-compact menu dropdown-content ml-2 mt-3 w-32 rounded-lg bg-gray-600 p-2 text-gray-50 shadow transition duration-300 ease-in-out"
            >
              <li>
                <a href="#home">Home</a>
              </li>
              <li>
                <a href="#services">Services</a>
              </li>
              <li>
                <a href="/privacy-policy">Privacy Policy</a>
              </li>
              <li>
                <a href="/terms">Terms Condition</a>
              </li>
            </ul>
          </div>
          <a href="/" className="flex cursor-pointer items-center">
            <img className="w-14" src="images/logo.png" alt="" />
            <h4 className="hidden font-mono text-2xl md:block">{settings?.company_name || 'BeGoal'}</h4>
          </a>
        </div>
        <div className="navbar-center hidden lg:flex">
          <ul className="menu menu-horizontal px-1 text-xl font-semibold transition duration-300 ease-in-out">
            <li>
              <a href="#home">Home</a>
            </li>
            <li>
              <a href="#services">Services</a>
            </li>
            <li>
              <a href="/privacy-policy">Privacy Policy</a>
            </li>
            <li>
              <a href="/terms">Terms Condition</a>
            </li>
          </ul>
        </div>
        <div className="navbar-end relative">
          <div className="absolute -right-1.5 top-1.5 z-10 h-12 w-32 rounded bg-gray-500 blur-sm"></div>
          <a
            href="#contact"
            className="z-30 flex h-12 w-32 items-center justify-center rounded border-2 border-black bg-white font-medium text-gray-800 shadow transition duration-300 ease-in hover:bg-gray-800 hover:text-gray-50 md:shadow"
          >
            Contact Us
          </a>
        </div>
      </div> */}
    </header>
  )
}
