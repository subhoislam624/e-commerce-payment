export default function ProductSection() {
  return (
    <section id="product" className="mt-10 px-1 md:px-10">
      <div className="grid grid-cols-1 md:grid-cols-2">
        <div className="flex items-center justify-center">
          <img src="images/products/products (2).png" alt="" />
        </div>
        <div className="flex flex-col items-start justify-center gap-4 p-4 md:p-10">
          <h4 className="text-3xl font-medium text-gray-900 dark:text-gray-50 md:text-4xl">
            Transforming Your Space into a Reflection of Your Personal Style
          </h4>
          <p className="text-gray-900 dark:text-gray-400">
            Our team of expert interior designers will work closely with you to create a personalized design plan that fits your lifestyle and budget.
            From consultation to project management, we guide you through every step of the design process to ensure that the end result is exactly
            what you envisioned. Let us help you design the home of your dreams.{' '}
          </p>
          <div className="flex gap-4">
            <a className="z-20 flex h-12 w-32 items-center justify-center rounded border border-black bg-gray-800 font-medium text-gray-50  shadow transition duration-300 ease-in hover:bg-gray-300 hover:text-black dark:bg-gray-400">
              Get Started
            </a>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2">
        <div className="flex flex-col items-start justify-center gap-4 p-4 md:p-10">
          <h4 className="text-3xl font-medium text-gray-900 dark:text-gray-50 md:text-4xl">
            Creating a Space that Reflects Your Personal Taste and Needs
          </h4>
          <p className="text-gray-900 dark:text-gray-400">
            At A Home Studio, we pride ourselves on providing exceptional service and delivering beautiful, functional designs. Our team of
            professionals will guide you through every step of the design process, ensuring your complete satisfaction. From consultation to project
            management, styling, and virtual design, we have the expertise to bring your vision to life. Contact us today and let{"'"}s make your
            dream home a reality.
          </p>
          <div className="flex gap-4">
            <a className="z-20 flex h-12 w-32 items-center justify-center rounded border border-black bg-gray-800 font-medium text-gray-50 shadow transition duration-300 ease-in hover:bg-gray-300 hover:text-black dark:bg-gray-400">
              Contact Us
            </a>
          </div>
        </div>
        <div className="flex items-center justify-center">
          <img src="images/products/products (6).png" alt="" />
        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2">
        <div className="flex items-center justify-center">
          <img src="images/products/products (1).png" alt="" />
        </div>
        <div className="flex flex-col items-start justify-center gap-4 p-4 md:p-10">
          <h4 className="text-3xl font-medium text-gray-900 dark:text-gray-50 md:text-4xl">Create the Perfect Ambiance with Lighting Design</h4>
          <p className="text-gray-900 dark:text-gray-400">
            Lighting has the power to transform a space, setting the mood and creating the perfect ambiance. Our team of lighting experts will work
            with you to choose the right fixtures and create a beautiful, functional lighting plan that enhances your space and enhances your life.
            From recessed lighting and chandeliers to wall sconces and pendants, we{''}ll help you create a space that{"'"}s both beautiful and
            functional.{' '}
          </p>
          <div className="flex gap-4">
            <a className="z-20 flex h-12 w-32 items-center justify-center rounded border border-black bg-gray-800 font-medium text-gray-50 shadow transition duration-300 ease-in hover:bg-gray-300 hover:text-black dark:bg-gray-400">
              Pick Lighting
            </a>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2">
        <div className="flex flex-col items-start justify-center gap-4 p-4 md:p-10">
          <h4 className="text-3xl font-medium text-gray-900 dark:text-gray-50 md:text-4xl">Bring Your Outdoor Living Space to Life</h4>
          <p className="text-gray-900 dark:text-gray-400">
            Make the most of your outdoor living space with our expert design services. Whether you{"'"}re looking to create a beautiful patio, an
            outdoor kitchen, or a cozy fire pit, our team of experts can help you bring your vision to life. We{''}ll work with you to choose the
            right materials, design features, and plants that will create a space that you{"'"}ll love spending time in. From landscape design to
            outdoor furniture, let us help you create a space that{"'"}s truly an extension of your home.
          </p>
          <div className="flex gap-4">
            <a className="z-20 flex h-12 w-32 items-center justify-center rounded border border-black bg-gray-800 font-medium text-gray-50 shadow transition duration-300 ease-in hover:bg-gray-300 hover:text-black dark:bg-gray-400">
              Outdoor Design
            </a>
          </div>
        </div>
        <div className="flex items-center justify-center">
          <img src="images/products/products (7).png" alt="" />
        </div>
      </div>
    </section>
  )
}
