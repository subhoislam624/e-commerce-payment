'use client'

import Link from 'next/link'
import { useGetCategoryQuery } from '@/features/web/category/userApi'
import Search from '../(home)/component/Search'
import CategoryCard from '../(home)/component/CategoryCard'
import Navbar from '../(home)/component/Navbar'
export default function Page() {
  const { data: categoryData, isLoading: categoryLoading, isError: categoryError } = useGetCategoryQuery({})

  const categoryLoadingElements = Array.from({ length: 12 }).map((_, index) => (
    <div key={index} role="status" className="animate-pulse overflow-hidden border-solid">
      <div className="mb-4 h-60 w-48 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      <span className="sr-only">Loading...</span>
    </div>
  ))

  return (
    <div className="w-full">
      <div className="mx-auto h-full min-h-[80vh] max-w-[1140px] p-2 ">
        <div>
          <Navbar />
        </div>
        <div className=" mx-auto mb-2 mt-5 w-full max-w-2xl">
          <h1 className="mb-3 text-base font-semibold opacity-60">What are you looking for?</h1>
          <Search />
        </div>

        <h1 className="pb-4 pt-4 text-xl font-semibold">All Category</h1>
        {categoryLoading ? (
          <div className="grid grid-cols-2 gap-4 pb-8 md:grid-cols-4 lg:grid-cols-6">{categoryLoadingElements}</div>
        ) : (
          <div className="grid w-full grid-cols-2 gap-4 pb-8 md:grid-cols-4 lg:grid-cols-6">
            {categoryData?.data?.slice(0, 20).map((category: any) => <CategoryCard key={category.id} category={category} />)}
          </div>
        )}
      </div>
    </div>
  )
}
