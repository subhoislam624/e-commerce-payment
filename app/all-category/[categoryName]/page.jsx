import CategoryProductDetails from './../[categoryName]/component/CategoryProductDetails'
export default function page({params }) {
  const { categoryName } = params
  return (
   <>
    <CategoryProductDetails categoryName={categoryName} />  
   </>
  )
}
