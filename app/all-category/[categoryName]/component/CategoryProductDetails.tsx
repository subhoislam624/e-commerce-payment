'use client'
import CategoryProduct from '@/app/(home)/component/CategoryProduct'
import Navbar from '@/app/(home)/component/Navbar'
import { useCategoryProductQuery } from '@/features/web/category/userApi'
import { useSearchParams } from 'next/navigation'
import { FcOk } from 'react-icons/fc'
import { AdvancedCheckbox, Text } from 'rizzui'

export default function CategoryProductDetails({ categoryName }: any) {
  const searchParams = useSearchParams()

  const id = searchParams.get('id')

  const { data: categoryData, isLoading } = useCategoryProductQuery(id)
  console.log(categoryData, 'categoryData')

  const categoryLoadingElements = Array.from({ length: 8 }).map((_, index) => (
    <div key={index} role="status" className="flex animate-pulse items-center justify-between overflow-hidden border-solid">
      <div className="flex">
        <div className="mb-4 h-20 w-32 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
        <div role="status" className="animate-pulse overflow-hidden border-solid">
          <div className="mb-2 ml-3 mt-3 h-6 w-32 rounded-sm bg-[#808080] dark:bg-gray-700 "></div>
          <div className="mb-4 ml-3 h-6 w-32 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
        </div>
      </div>
      <div className="flex ">
        <div className="mb-2 ml-3 h-6 w-32 rounded-sm bg-[#808080] dark:bg-gray-700 "></div>
        <div className="mb-4 ml-3 h-6 w-32 rounded-sm bg-[#808080] dark:bg-gray-700"></div>
      </div>
      <span className="sr-only">Loading...</span>
    </div>
  ))

  return (
    <div className="w-full">
      <div className="mx-auto h-full max-w-[1140px] p-2 ">
        <div>
          <Navbar />
        </div>
        <h1 className="pb-4 pt-8 text-xl font-semibold">({categoryName}) Products</h1>
        <div className="grid w-full grid-flow-row gap-5 pb-5 lg:grid-cols-3 ">
          <div
            className={`border-t border-[#D7DFE1] lg:col-span-2 ${categoryData?.data?.length == 0 ? 'flex w-full items-center justify-center' : ''}`}
          >
            {isLoading ? (
              <div className="mt-4">{categoryLoadingElements}</div>
            ) : (
              <>
                {categoryData?.data?.length == 0 ? (
                  <div className="w-full">
                    <AdvancedCheckbox name="currency" value="taka" defaultChecked>
                      <Text className="py-10 text-center font-semibold">No Data Found</Text>
                    </AdvancedCheckbox>
                  </div>
                ) : (
                  categoryData?.data?.map((categoryProduct: any) => <CategoryProduct key={categoryProduct.id} product={categoryProduct} />)
                )}
              </>
            )}
          </div>

          <div className="lg:col-span-1">
            <div className="mb-6 rounded-md border border-[#D7DFE1] p-3">
              <h1 className="flex items-center gap-x-2 pb-1 text-base font-semibold">
                <FcOk /> Instant delivery
              </h1>
              <p className="text-sm">You will receive the code directly by email, so that you can use the credit immediately.</p>
            </div>
            <div className="mb-6 rounded-md border border-[#D7DFE1] p-3">
              <h1 className="flex items-center gap-x-2 pb-1 text-base font-semibold">
                <FcOk /> Gift card design
              </h1>
              <p className="text-sm">You will receive the code directly by email, so that you can use the credit immediately.</p>
            </div>
            <div className="mb-6 rounded-md border border-[#D7DFE1] p-3">
              <h1 className="flex items-center gap-x-2 pb-1 text-base font-semibold">
                <FcOk /> Instant delivery
              </h1>
              <p className="text-sm">
                You will receive the code directly by email, so that you can use the credit immediatelyYou will receive the code directly by email, so
                that you can use the credit immediately.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
