import StoreProvider from '@/features/context/store-provider'
import '@/styles/tailwind.css'
import '@radix-ui/themes/styles.css'
import { Marcellus, Poppins } from 'next/font/google'
import NextTopLoader from 'nextjs-toploader'
import { Toaster } from 'react-hot-toast'
import GlobalModal from './admin/components/deleteModal/container'

const poppins = Poppins({
  subsets: ['latin'],
  weight: ['100', '200', '300', '400', '500', '600', '700', '800', '900'],
  display: 'swap',
  style: ['normal', 'italic'],
})

const marcellus = Marcellus({
  subsets: ['latin'],
  weight: ['400'],
  display: 'swap',
  variable: '--font-marcellus',
})

export async function generateMetadata() {
  // const session = await getServerSession(authOptions)
  // const res = await api('/administration-settings', {
  //   token: session?.accessToken,
  // })
  // if (res.status) {
  //   return {
  //     title: res?.data?.site_title,
  //     icons: {
  //       icon: res?.data?.site_icon || undefined,
  //     },
  //   }
  // } else {
  //   return {
  //     title: 'Joltori',
  //     icons: {
  //       icon: undefined,
  //     },
  //   }
  // }
}

export default async function RootLayout({ children }) {
  // const session = await getServerSession(authOptions)

  return (
    <html lang="en" className={`${poppins.className} ${marcellus.variable}`}>
      <body suppressHydrationWarning={true}>
        <NextTopLoader showSpinner={false} />
        <StoreProvider>{children}</StoreProvider>
        <GlobalModal />
        <Toaster position="top-right" reverseOrder={false} />
      </body>
    </html>
  )
}
