export interface ICategory {
  id?: string
  name: string
  image: string
  status: Boolean
  isPopular: Boolean
}

export interface IProduct {
  id?: string
  name: string
  image: string
  status: boolean
  isPopular: boolean
  price: number
  category: string
  quantity: number
  description?: string
}
