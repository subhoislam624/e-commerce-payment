import { withAuth } from 'next-auth/middleware'
import { NextResponse } from 'next/server'

export default withAuth(
  function middleware(req) {
    const { pathname } = req.nextUrl
    const { token } = req.nextauth
    console.log(token, 'token')
    // if (token?.accessToken && pathname.startsWith('/admin-login')) {
    //   return NextResponse.redirect(new URL('/admin/dashboard', req.url))
    // } else if (!token?.accessToken && !pathname.startsWith('/admin-login')) {
    //   return NextResponse.redirect(new URL('/admin-login', req.url))
    // }
  },
  {
    callbacks: {
      authorized: ({ token }) => true,
    },
  },
)

export const config = {
  matcher: ['/admin/:path*', '/admin-login'],
}
